//
//  SSTabBarController.m
//  SSTabBarController
//
//  Created by Susim on 6/27/14.
//  Copyright (c) 2014 Susim. All rights reserved.
//

#import "SSTabBarController.h"
#import "AppDelegate.h"


@interface SSTabBarController ()
@property (nonatomic,weak)  UIView *customTabContainer;
- (IBAction)selectedTab:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imgRes;
@property (weak, nonatomic) IBOutlet UIImageView *imgMap;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;

@end

@implementation SSTabBarController
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   // [self hideExistingTabBar];
    NSArray *nibObjects = [[NSBundle mainBundle] loadNibNamed:@"SSTabBarController" owner:self options:nil];
    self.customTabContainer = [nibObjects objectAtIndex:0];
    
    //add bar
//    UILabel *lblGray = [[UILabel alloc]initWithFrame:CGRectMake(0, (self.view.frame.size.height-self.customTabContainer.frame.size.height) - 1 ,self.view.frame.size.width, 1)];
//    lblGray.backgroundColor = [UIColor lightGrayColor];
//    
//     [self.view addSubview:lblGray];
    
    self.customTabContainer.frame = CGRectMake(0, self.view.frame.size.height-self.customTabContainer.frame.size.height, self.view.frame.size.width, self.customTabContainer.frame.size.height);
    [self.view addSubview:self.customTabContainer];
   [self selectedTab:self.customTabContainer.subviews[1]];
   
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}
- (void)hideExistingTabBar {
	for(UIView *view in self.view.subviews){
		if([view isKindOfClass:[UITabBar class]]){
			view.hidden = YES;
			break;
        }
    }
    self.customTabContainer.hidden = YES;
}
- (void) processCompleted:(NSInteger)btnTag{
    [self setSelectedIndex:btnTag];
}
- (IBAction)selectedTab:(id)sender {
    
    
    UIButton *tabBtn = (UIButton *)sender;
    if(tabBtn.tag==0){
        self.btnHome.selected = YES;
        
        self.btnCasino.selected = NO;
         self.btnDining.selected = NO;
         self.btnEvent.selected = NO;
         self.btnHotel.selected = NO;
         self.btnOffer.selected = NO;
    }else if (tabBtn.tag==1){
        self.btnCasino.selected = YES;
        
        self.btnHome.selected = NO;
        self.btnDining.selected = NO;
        self.btnEvent.selected = NO;
        self.btnHotel.selected = NO;
        self.btnOffer.selected = NO;

    }else if (tabBtn.tag==2){
        self.btnDining.selected = YES;
        
        self.btnHome.selected = NO;
        self.btnCasino.selected = NO;
        self.btnEvent.selected = NO;
        self.btnHotel.selected = NO;
        self.btnOffer.selected = NO;
       
    }else if (tabBtn.tag==3){
        self.btnEvent.selected = YES;
        
        self.btnHome.selected = NO;
        self.btnCasino.selected = NO;
        self.btnDining.selected = NO;
        self.btnHotel.selected = NO;
        self.btnOffer.selected = NO;
        
    }else if (tabBtn.tag==4){
        self.btnHotel.selected = YES;
        
        self.btnHome.selected = NO;
        self.btnDining.selected = NO;
        self.btnEvent.selected = NO;
        self.btnCasino.selected = NO;
        self.btnOffer.selected = NO;
    }else if (tabBtn.tag==5){
        self.btnOffer.selected = YES;
        
        self.btnHome.selected = NO;
        self.btnDining.selected = NO;
        self.btnEvent.selected = NO;
        self.btnHotel.selected = NO;
        self.btnCasino.selected = NO;
    }
    
    [self setSelectedIndex:tabBtn.tag];
    
}
@end
