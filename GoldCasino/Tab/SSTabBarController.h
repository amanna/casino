//
//  SSTabBarController.h
//  SSTabBarController
//
//  Created by Susim on 6/27/14.
//  Copyright (c) 2014 Susim. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol tabbardelegate <NSObject>
@end
@interface SSTabBarController : UITabBarController<tabbardelegate>{
    id <tabbardelegate> _delegate;
}
- (void)hideExistingTabBar;
- (void) processCompleted:(NSInteger)btnTag;
@property (nonatomic,strong) id delegate;
@property (weak, nonatomic) IBOutlet UIButton *btnCasino;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnDining;
@property (weak, nonatomic) IBOutlet UIButton *btnEvent;
@property (weak, nonatomic) IBOutlet UIButton *btnHotel;
@property (weak, nonatomic) IBOutlet UIButton *btnOffer;
@property(nonatomic)BOOL isTab;
@end
