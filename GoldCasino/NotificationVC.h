//
//  NotificationVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 23/02/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeNo {
    kNotDetails = 0,
    kNotBack=1
    
};
typedef void (^EventCompletionHandlerN)(id object, NSUInteger EventTypeNo);
@interface NotificationVC : UIViewController{
    EventCompletionHandlerN eventCompletionHandlerN;
}
- (void)setEventOnCompletion:(EventCompletionHandlerN)handler ;


@end
