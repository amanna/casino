//
//  SettingsVC.m
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "SettingsVC.h"
#import "POAcvityView.h"
#import "DatepickerView.h"
#import "WebServiceManager.h"
@interface SettingsVC (){
     POAcvityView *activity;
    
}

- (IBAction)btnMenuAction:(UIButton *)sender;
- (IBAction)btnNoticeAction:(UIButton *)sender;
- (IBAction)btnSubmitAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtBirth;
@property (weak, nonatomic) IBOutlet UILabel *lblCalendar;

- (IBAction)btnPushAction:(UIButton *)sender;
- (IBAction)btnCalendarAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;

@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (nonatomic) DatepickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIButton *btnLocYes;
@property (weak, nonatomic) IBOutlet UIButton *btnLocNo;
@property (nonatomic)NSString *strPush;
@property (nonatomic)NSString *strGps;
@property (nonatomic)NSString *strBday;

- (IBAction)btnLocationAccess:(UIButton *)sender;
@property(nonatomic)NSDictionary *dict;
@end

@implementation SettingsVC

- (void)viewDidAppear:(BOOL)animated{
    [activity showView];
    [WebServiceManager getUserSettings:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            self.dict = object;
            [self  loadUI];
        }else{
            
        }
    }];

}
- (void)loadUI{
    
    NSString *strbday = [self.dict valueForKey:@"IsReceive_Bday_Promotions"];
    BOOL bdayBool = [strbday boolValue];
    if(bdayBool==true){
        NSArray *arr = [[self.dict valueForKey:@"Date_Of_Birth"] componentsSeparatedByString:@" "];
        
        NSString *strDt = [arr objectAtIndex:0];
        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"MM/dd/yyyy";
        NSDate *yourDate = [dateFormatter dateFromString:strDt];
        
        [dateFormatter setDateFormat:@"MM-dd-yyyy"];
        self.lblCalendar.text = [dateFormatter stringFromDate:yourDate];
        
        
        self.strBday = @"1";
    }else{
        self.strBday = @"0";
        self.lblCalendar.text = @"";
    }
    
    
    self.txtEmail.text = [self.dict valueForKey:@"Email_Address"];
    self.txtPhone.text = [self.dict valueForKey:@"Telephone_Mobile"];
    
    NSString *strNotify = [self.dict valueForKey:@"IsSend_Notifications"];
    BOOL notify = [strNotify boolValue];
    
   
   //
    if(notify==true){
        [self.btnYes setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnNo setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strPush = @"1";
    }else{
        [self.btnNo setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnYes setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
         self.strPush = @"0";
    }
    
    //AccessGeoData
    
    NSString *strLoc = [self.dict valueForKey:@"AccessGeoData"];
    BOOL gpsBool = [strLoc boolValue];
    //
    if(gpsBool==true){
        [self.btnLocYes setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnLocNo setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strGps = @"1";
    }else{
        [self.btnLocNo setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnLocYes setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strGps = @"0";
    }

    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    
   
    self.txtEmail.autocorrectionType = UITextAutocorrectionTypeNo;
    self.txtPhone.autocorrectionType = UITextAutocorrectionTypeNo;
    
    UIStoryboard  *storyboard =
    [UIStoryboard storyboardWithName:@"Main"
                              bundle:nil];
    
    self.pickerView = [storyboard instantiateViewControllerWithIdentifier:@"DatepickerView"];
    
    [self.pickerView setEventOnCompletionDe:^(id object, NSUInteger EventTypeDateP) {
        if(EventTypeDateP == kPicOpen){
            NSDateFormatter *df = [[NSDateFormatter alloc] init];
            [df setDateFormat:@"MM-dd-yyyy"];
            self.lblCalendar.text = [NSString stringWithFormat:@"%@",
                                  [df stringFromDate:self.pickerView.datepicker.date]];
           
        }else if (EventTypeDateP == kPicClose){
            [self.pickerView.view removeFromSuperview];
        }else if (EventTypeDateP==kPicCancel){
             [self.pickerView.view removeFromSuperview];
        }
    }];
    
    
//    [self.myWeb loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://app.xgoldcasino.com/setting_new_app.html"]]];
    
    //[activity showView];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

    // Do any additional setup after loading the view.
}

- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    eventCompletionHandlerSt(@"1",kTapSt);
}
- (void)setEventOnCompletionSt:(EventCompletionHandlerSt)handler{
    eventCompletionHandlerSt = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)btnMenuAction:(UIButton *)sender {
   if(self.view.frame.origin.x==0){
        eventCompletionHandlerSt(@"0",kMenuSt);
    }else{
        eventCompletionHandlerSt(@"1",kTapSt);
    }
}

- (IBAction)btnNoticeAction:(UIButton *)sender {
     eventCompletionHandlerSt(nil,kStNotify);
}

- (IBAction)btnSubmitAction:(UIButton *)sender {
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    
    if ([emailTest evaluateWithObject:self.txtEmail.text] == NO)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Enter valid Email" message:@"Please Enter Valid Email Address." delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
        [alert show];
        
    }else{
    
    
        NSString *strBirth;
    if([self.txtBirth.text isEqualToString:@""]){
        self.strBday = @"0";
        strBirth = @"0";
    }else{
        self.strBday = @"1";
        strBirth = self.lblCalendar.text;
    }
    NSString *strUserInfo = [NSString stringWithFormat:@"%@/%@/%@/%@/%@/%@",self.strPush,self.strBday,self.strGps,strBirth,self.txtPhone.text,self.txtEmail.text];
    [activity showView];
    [WebServiceManager updateUserSettings:strUserInfo onCompletion:^(id object, NSError *error) {
        [activity hideView];
        if(object){
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Settings" message:@"Settings Information has been successfully updated" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];

        }else{
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Settings" message:@"Some Problem Occurs!!Please try again later" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [alertView show];
        }
    }];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnPushAction:(UIButton *)sender {
    if(sender.tag == 100){
        [self.btnYes setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnNo setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strPush= @"1";
    }else{
        [self.btnNo setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnYes setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strPush= @"0";
    }
}

- (IBAction)btnCalendarAction:(UIButton *)sender {
    [self.view addSubview:self.pickerView.view];
}
- (void)LabelChange:(id)sender{
    
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    if(textField==self.txtEmail){
        [self.txtEmail resignFirstResponder];
        [self.txtPhone becomeFirstResponder];
    }else{
        [self.txtPhone resignFirstResponder];
        
    }
    return YES;
}
- (IBAction)btnLocationAccess:(UIButton *)sender {
    if(sender.tag == 1000){
        [self.btnLocYes setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnLocNo setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strGps= @"1";
    }else{
        [self.btnLocNo setBackgroundImage:[UIImage imageNamed:@"grey_box_yes.png"] forState:UIControlStateNormal];
        [self.btnLocYes setBackgroundImage:[UIImage imageNamed:@"grey_box_no.png"] forState:UIControlStateNormal];
        self.strGps= @"0";
    }
}
@end
