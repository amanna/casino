//
//  ReservationVC.m
//  GoldCasino
//
//  Created by Amrita on 30/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "ReservationVC.h"
#import "POAcvityView.h"

@interface ReservationVC (){
     POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UIWebView *myWebview;
- (IBAction)btnBackAction:(UIButton *)sender;
@end

@implementation ReservationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
     [activity showView];
    NSURLRequest *requestObject = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://app.xgoldcasino.com/xgold-book/index.html"] cachePolicy:NSURLCacheStorageAllowed timeoutInterval:1800]; // timeoutInterval is in seconds
    
    
    [self.myWebview loadRequest:requestObject];
    

   
   

    // Do any additional setup after loading the view.
}
- (void)setEventOnCompletionRe:(EventCompletionHandlerRe)handler{
    eventCompletionHandlerRe = handler;
}
- (IBAction)btnBackAction:(UIButton *)sender {
    eventCompletionHandlerRe(nil,kReBack);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
    
    [activity hideView];
    NSLog(@"finish");
}
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    
    [activity showView];
    NSLog(@"Error for WEBVIEW: %@", [error description]);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
