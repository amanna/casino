//
//  ScannerVC.m
//  POP
//
//  Created by Tuli on 6/10/14.
//  Copyright (c) 2014 Tuli e services. All rights reserved.
//

#import "ScannerVC.h"

#import "WebServiceManager.h"

@interface ScannerVC ()<UIAlertViewDelegate>{
    /* Here’s a quick rundown of the instance variables (via 'iOS 7 By Tutorials'):
     
     1. _captureSession – AVCaptureSession is the core media handling class in AVFoundation. It talks to the hardware to retrieve, process, and output video. A capture session wires together inputs and outputs, and controls the format and resolution of the output frames.
     
     2. _videoDevice – AVCaptureDevice encapsulates the physical camera on a device. Modern iPhones have both front and rear cameras, while other devices may only have a single camera.
     
     3. _videoInput – To add an AVCaptureDevice to a session, wrap it in an AVCaptureDeviceInput. A capture session can have multiple inputs and multiple outputs.
     
     4. _previewLayer – AVCaptureVideoPreviewLayer provides a mechanism for displaying the current frames flowing through a capture session; it allows you to display the camera output in your UI.
     5. _running – This holds the state of the session; either the session is running or it’s not.
     6. _metadataOutput - AVCaptureMetadataOutput provides a callback to the application when metadata is detected in a video frame. AV Foundation supports two types of metadata: machine readable codes and face detection.
     7. _backgroundQueue - Used for showing alert using a separate thread.
     */
    AVCaptureSession *_captureSession;
    AVCaptureDevice *_videoDevice;
    AVCaptureDeviceInput *_videoInput;
    AVCaptureVideoPreviewLayer *_previewLayer;
    BOOL _running;
    AVCaptureMetadataOutput *_metadataOutput;
    
    BOOL isShow;
    
     UIView *_centerView;
}
@property (strong, nonatomic) NSMutableArray * foundBarcodes;
@property (weak, nonatomic) IBOutlet UIView *previewView;
- (IBAction)btnCancelAction:(id)sender;

@end

@implementation ScannerVC
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)setEventOnCompletionSC:(EventCompletionHandlerSc)handler{
     eventCompletionHandlerSc = handler;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _centerView = [[UIView alloc] init];
    

    
    _centerView.frame = CGRectMake(self.view.frame.size.width/2 -160, self.view.frame.size.height/2 - 160, 320, 320);
    _centerView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    
    
    UIGraphicsBeginImageContext(self.view.frame.size);
    [[UIImage imageNamed:@"scanner.png"] drawInRect:_centerView.bounds];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    //[UIColor colorWithPatternImage:image]
    _centerView.backgroundColor = [UIColor redColor];
    [self.view addSubview:_centerView];
    
    
    
    
    isShow = YES;
    [self setupCaptureSession];
    _previewLayer.frame = CGRectMake(0, 0, 414, 600);
    [_previewView.layer addSublayer:_previewLayer];
    self.foundBarcodes = [[NSMutableArray alloc] init];
    
    // listen for going into the background and stop the session
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(applicationWillEnterForeground:)
     name:UIApplicationWillEnterForegroundNotification
     object:nil];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(applicationDidEnterBackground:)
     name:UIApplicationDidEnterBackgroundNotification
     object:nil];
    
    // set default allowed barcode types, remove types via setings menu if you don't want them to be able to be scanned
    self.allowedBarcodeTypes = [NSMutableArray new];
    [self.allowedBarcodeTypes addObject:@"org.iso.QRCode"];
//    [self.allowedBarcodeTypes addObject:@"org.iso.PDF417"];
//    [self.allowedBarcodeTypes addObject:@"org.gs1.UPC-E"];
//    [self.allowedBarcodeTypes addObject:@"org.iso.Aztec"];
//    [self.allowedBarcodeTypes addObject:@"org.iso.Code39"];
//    [self.allowedBarcodeTypes addObject:@"org.iso.Code39Mod43"];
//    [self.allowedBarcodeTypes addObject:@"org.gs1.EAN-13"];
//    [self.allowedBarcodeTypes addObject:@"org.gs1.EAN-8"];
//    [self.allowedBarcodeTypes addObject:@"com.intermec.Code93"];
//    [self.allowedBarcodeTypes addObject:@"org.iso.Code128"];
     [self.view bringSubviewToFront:_centerView];
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self startRunning];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self stopRunning];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - AV capture methods

- (void)setupCaptureSession {
    // 1
    if (_captureSession) return;
    // 2
    _videoDevice = [AVCaptureDevice
                    defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (!_videoDevice) {
        NSLog(@"No video camera on this device!");
        return;
    }
    // 3
    _captureSession = [[AVCaptureSession alloc] init];
    // 4
    _videoInput = [[AVCaptureDeviceInput alloc]
                   initWithDevice:_videoDevice error:nil];
    // 5
    if ([_captureSession canAddInput:_videoInput]) {
        [_captureSession addInput:_videoInput];
    }
    // 6
    _previewLayer = [[AVCaptureVideoPreviewLayer alloc]
                     initWithSession:_captureSession];
    _previewLayer.videoGravity =
    AVLayerVideoGravityResizeAspectFill;
    
    
    // capture and process the metadata
    _metadataOutput = [[AVCaptureMetadataOutput alloc] init];
    dispatch_queue_t metadataQueue =
    dispatch_queue_create("com.1337labz.featurebuild.metadata", 0);
    [_metadataOutput setMetadataObjectsDelegate:self
                                          queue:metadataQueue];
    if ([_captureSession canAddOutput:_metadataOutput]) {
        [_captureSession addOutput:_metadataOutput];
    }
}

- (void)startRunning {
    if (_running) return;
    [_captureSession startRunning];
    _metadataOutput.metadataObjectTypes =
    _metadataOutput.availableMetadataObjectTypes;
    _running = YES;
}
- (void)stopRunning {
    if (!_running) return;
    [_captureSession stopRunning];
    _running = NO;
}

//  handle going foreground/background
- (void)applicationWillEnterForeground:(NSNotification*)note {
    [self startRunning];
}
- (void)applicationDidEnterBackground:(NSNotification*)note {
    [self stopRunning];
}

#pragma mark - Delegate functions

- (void)captureOutput:(AVCaptureOutput *)captureOutput
didOutputMetadataObjects:(NSArray *)metadataObjects
       fromConnection:(AVCaptureConnection *)connection
{
    if(isShow){
    if(metadataObjects.count >0){
    AVMetadataObject *obj = [metadataObjects objectAtIndex:0];
    
    if ([obj isKindOfClass:
         [AVMetadataMachineReadableCodeObject class]])
    {
        // 3
        AVMetadataMachineReadableCodeObject *code =
        (AVMetadataMachineReadableCodeObject*)
        [_previewLayer transformedMetadataObjectForMetadataObject:obj];
        // 4
        
        Barcode * barcode = [Barcode processMetadataObject:code];
        
        isShow = NO;
        
        for(NSString * str in self.allowedBarcodeTypes){
            if([barcode.getBarcodeType isEqualToString:str]){
                [self validBarcodeFound:barcode];
                return;
            }
        }
    }
    }
    }
}

- (void) validBarcodeFound:(Barcode *)barcode{
    [self stopRunning];
    [self.foundBarcodes addObject:barcode];
    [self showBarcodeAlert:barcode];
}
- (void) showBarcodeAlert:(Barcode *)barcode{
     NSString *offerNo = [barcode getBarcodeData];
    [WebServiceManager scanOfferCode:offerNo onCompletion:^(id object, NSError *error) {
        if(object){
            NSString *str = object;
            if([str isEqualToString:@"1"]){
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Offer Code"
                                                                  message:@"This offer code succesfully saved in My Offer"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Done"
                                                        otherButtonTitles:@"Scan again",nil];
                message.tag=2000;
                [message show];

            }else if ([str isEqualToString:@"2"]){
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Offer Code"
                                                                  message:@"This offer is already in your  Offer"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Done"
                                                        otherButtonTitles:@"Scan again",nil];
                [message show];
            }else{
                UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Offer Code"
                                                                  message:@"Invalid Offer Code"
                                                                 delegate:self
                                                        cancelButtonTitle:@"Done"
                                                        otherButtonTitles:@"Scan again",nil];
                [message show];
            }
           
        }else{
            UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Offer Code"
                                                              message:@"Some problem occurs!!Please try again later"
                                                             delegate:self
                                                    cancelButtonTitle:@"Done"
                                                    otherButtonTitles:@"Scan again",nil];
            [message show];
        }
    }];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if(alertView.tag ==2000){
        if(buttonIndex == 0){
            isShow = YES;
             eventCompletionHandlerSc(nil,kScanBack);
        }else{
            isShow = YES;
            //Code for Scan more button
            [self startRunning];
        }
    }else{
 if(buttonIndex == 0){
     isShow = YES;
     [self startRunning];
     //Code for Done button
     // TODO: Create a finished view
 }
 if(buttonIndex == 1){
     isShow = YES;
     //Code for Scan more button
     [self startRunning];
}
    }

}

-(void)showAlertWithError{
    UIAlertView *alert  = [[UIAlertView alloc]initWithTitle:@"Goldcasino"  message:@"Please try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil,nil];
    [alert show];
    alert.delegate = self;
    alert.tag = 4500;
    
}


- (IBAction)btnbackAction:(UIButton *)sender {
    eventCompletionHandlerSc(nil,kScanBack);
}

- (IBAction)btnCancelAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
