//
//  NightLife.h
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeN {
    kNightTapMenu = 0,
    kNightCasino = 1,
    kNightDining = 2,
    kNightEvent = 3,
    kNightHotel = 4,
    kNightNight = 5,
    kNightTap=6,
    kNightDetails=7,
    kNightNotify
};
typedef void (^EventCompletionHandlerN)(id object, NSUInteger EventTypeN);

@interface NightLife : UIViewController{
    EventCompletionHandlerN eventCompletionHandlerN;
}
- (void)setEventOnCompletion:(EventCompletionHandlerN)handler ;
@property (weak, nonatomic) IBOutlet UIView *viewTrans;
@property(nonatomic)NSString *strSectionId;
@end
