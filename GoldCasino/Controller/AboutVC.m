//
//  AboutVC.m
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AboutVC.h"
#import <MapKit/MapKit.h>
@interface AboutVC ()

- (IBAction)btnMenuAction:(UIButton *)sender;
- (IBAction)btnNoticeAction:(UIButton *)sender;

@end

@implementation AboutVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:singleFingerTap];

    // Do any additional setup after loading the view.
    
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
    eventCompletionHandlerA(@"1",kAboutTap);
}
- (void)setEventOnCompletionS:(EventCompletionHandlerA)handler{
     eventCompletionHandlerA = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)btnMenuAction:(UIButton *)sender {
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerA(@"0",kAboutMenu);
    }else{
        eventCompletionHandlerA(@"1",kAboutTap);
    }
}

- (IBAction)btnNoticeAction:(UIButton *)sender {
    eventCompletionHandlerA(nil,kAboutNotify);
    
}
@end
