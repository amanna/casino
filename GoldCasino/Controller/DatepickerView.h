//
//  DatepickerView.h
//  GoldCasino
//
//  Created by MAPPS MAC on 18/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeDateP {
    kPicClose = 0,
    kPicOpen = 1,
    kPicCancel=2
   
};
typedef void (^EventCompletionHandlerPicker)(id object, NSUInteger EventTypeDateP);
@interface DatepickerView : UIViewController{
    EventCompletionHandlerPicker eventCompletionHandlerPic;
}
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;
- (void)setEventOnCompletionDe:(EventCompletionHandlerPicker)handler;
@end






