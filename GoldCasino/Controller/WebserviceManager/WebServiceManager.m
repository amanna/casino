//
//  WebServiceManager.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Wovax, LLC. All rights reserved.
//

#import "WebServiceManager.h"
#import "SSRestManager.h"
#import "AppConstants.h"
#import "Offer.h"
#import "Section.h"
#import "Subsection.h"
#import "Pages.h"
@implementation WebServiceManager
+(void)loginOnCompletion:(NSString*)strUserName andPass:(NSString*)strPass onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kLogin];
//    NSString *kQuery = [NSString stringWithFormat:@"%@",@"/27CCC3A558E542DD827649EED27F64C3/tkohli@easypurl.com/test123/1234/0"];
    //strUserName=@"tkohli@easypurl.com";
    //strPass = @"test123";
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUDID = [prefs stringForKey:@"udid"];
    NSString *strDeviceToken = [prefs stringForKey:@"deviceToken"];
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/%@/%@/%@/%@/1",strUserName,strPass,strUDID,strDeviceToken];
    
       SSRestManager *restManager = [[SSRestManager alloc]init];
       [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
           if(json){
              BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
               if(strSucces == true){
                   NSDictionary *dictPost = [json valueForKey:@"User_Details"];
                   NSString *strName = [dictPost valueForKey:@"Name"];
                   NSString *strTarget = [dictPost valueForKey:@"TargetId"];
                   NSInteger upt = [[dictPost valueForKey:@"UserPoints"]integerValue];
                   NSString *strUserPoint = [NSString stringWithFormat:@"%ld",upt];
                   NSString *strTire = [dictPost valueForKey:@"Tire"];
                   NSString *strPlayer = [dictPost valueForKey:@"PlayerId"];
                   
                   [[NSUserDefaults standardUserDefaults] setObject:strName forKey:@"username"];
                   [[NSUserDefaults standardUserDefaults] setObject:strTarget forKey:@"target"];
                   [[NSUserDefaults standardUserDefaults] setObject:strUserPoint forKey:@"userpoints"];
                   [[NSUserDefaults standardUserDefaults] setObject:strTire forKey:@"Tire"];
                    [[NSUserDefaults standardUserDefaults] setObject:strPlayer forKey:@"PlayerId"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   handler (@"1",nil);
               }else{
                   handler (@"0",nil);
               }
           }
          } onError:^(NSError *error) {
              if (error) {
                  handler(nil,error);
              }
           
       }];

}
+(void)saveMyDeviceToken:(NSString*)strOffer onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSaveDeviceToken];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUDID = [prefs stringForKey:@"udid"];
     NSString *strDeviceToken = [prefs stringForKey:@"deviceToken"];
    NSString *target = [prefs stringForKey:@"target"];
    // target = @"2023";
    if(target==NULL){
        target=@"0";
    }
    NSString *kQuery = [NSString stringWithFormat:@"/%@/%@/%@/27CCC3A558E542DD827649EED27F64C3",strUDID,strDeviceToken,target];
    //http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
            NSMutableArray *offerFullList = [[NSMutableArray alloc] init];
            if(strSucces == true){
                NSArray *offerList = [json valueForKey:@"OfferList"];
                [offerList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
                    Offer *offer=[[Offer alloc]initWithDictionary:menuDict];
                    [offerFullList addObject:offer];
                    
                }];
                handler (offerFullList,nil);
            }else{
                handler (offerFullList,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+(void)getSectionWisePageListOnCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSectionList];
    
    NSString *kQuery = @"/27CCC3A558E542DD827649EED27F64C3/1";
    
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSArray *sectionArray = [json valueForKey:@"sectionList"];
            NSMutableArray *arrSection = [[NSMutableArray alloc]init];
            if(sectionArray.count >0){
                [sectionArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                    Section *offer=[[Section alloc]initWithDictionary:dict];
                    [arrSection addObject:offer];
                }];
                
                handler (arrSection,nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)getPageDetailsOnCompletion:(NSString *)strPage onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kPageContentDetails];
    
   NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/1/%@",strPage];
    
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSArray *sectionArray = [json valueForKey:@"sectionList"];
            NSMutableArray *arrSection = [[NSMutableArray alloc]init];
            if(sectionArray.count >0){
                [sectionArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                    Section *offer=[[Section alloc]initWithDictionary:dict];
                    [arrSection addObject:offer];
                }];
                
                handler (arrSection,nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)getPageListOnCompletion:(NSString *)strSection onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSectionPageList];
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/1/%@",strSection];
    
    SSRestManager *restManager = [[SSRestManager alloc]init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if(json){
            NSArray *sectionArray = [json valueForKey:@"subsectionListWithPage"];
            NSMutableArray *arrSection = [[NSMutableArray alloc]init];
            if(sectionArray.count >0){
                [sectionArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
                    
                    Pages *page=[[Pages alloc]init];
                    page.strSubTitle = [dict valueForKey:@"SubsectionTitle"];
                    page.strPageType= @"header";
                    [arrSection addObject:page];
                    
                    NSArray *pageList = [dict valueForKey:@"memPageList"];
                    [pageList enumerateObjectsUsingBlock:^(NSDictionary *dict1, NSUInteger idx, BOOL * _Nonnull stop) {
                        Pages *page=[[Pages alloc]init];
                        page.strPageId= [dict1 valueForKey:@"PageId"];
                        page.strPageDesc= [dict1 valueForKey:@"PageContent"];
                        page.strPageTitle= [dict1 valueForKey:@"PageTitle"];
                        page.strPageThumbImg= [dict1 valueForKey:@"PageThumbImg"];
                        page.strPageMainImage= [dict1 valueForKey:@"PageMainImage"];
                        page.strPageType= @"page";
                        [arrSection addObject:page];

                    }];
                    
                }];
                
                handler (arrSection,nil);
            }else{
                handler (@"0",nil);
            }
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
        
    }];

}
+(void)saveMyLatLongitude:(NSString*)strLatt andLoni:(NSString*)strLoni onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kLocDeviceToken];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
   
    if(target==NULL){
        target=@"0";
    }
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh-mm-ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *strUDID = [prefs stringForKey:@"udid"];
    // target = @"2023";
    NSString * timestamp = [dateFormatter stringFromDate:[NSDate date]];
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/%@/%@/%@/%@/%@/1",strLatt,strLoni,target,timestamp,strUDID];
    //http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
            NSMutableArray *offerFullList = [[NSMutableArray alloc] init];
            if(strSucces == true){
                NSArray *offerList = [json valueForKey:@"OfferList"];
                [offerList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
                    Offer *offer=[[Offer alloc]initWithDictionary:menuDict];
                    [offerFullList addObject:offer];
                    
                }];
                handler (offerFullList,nil);
            }else{
                handler (offerFullList,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+(void)getOfferListOnCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kAllOfferList];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
   // target = @"2023";
    
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/%@/0",target];
//http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0

    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
           if (json) {
               BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
               NSMutableArray *offerFullList = [[NSMutableArray alloc] init];
               if(strSucces == true){
                   NSArray *offerList = [json valueForKey:@"OfferList"];
                   [offerList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
                       Offer *offer=[[Offer alloc]initWithDictionary:menuDict];
                       [offerFullList addObject:offer];
                       
                   }];
                   handler (offerFullList,nil);
               }else{
                   handler (offerFullList,nil);
               }
               
            }
       } onError:^(NSError *error) {
           if (error) {
               handler(nil,error);
           }
       }];

}
+(void)getUserSettings :(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kGetUserSettings];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
    //target = @"2022";
    
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/2/44626/%@",target];
    //http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
           
            handler(json,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+(void)updateUserSettings :(NSString *)strUser onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kUpdateUserSettings];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
    //target = @"2023";
    
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/2/44626/%@/%@",target,strUser];
    //http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSInteger strSucces = [[json valueForKey:@"IsSuccess"]integerValue];
            if(strSucces ==1){
                handler(@"1",nil);
            }else{
                handler(@"0",nil);
            }
            
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+(void)getMyOfferListOnCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kMyOfferList];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
    //target = @"2023";
    
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/%@/0",target];
    //http://api.experiture.com/rest/v1/json/getOfferDetailListRest/27CCC3A558E542DD827649EED27F64C3/22.4696554/88.393355/2023/0
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
            NSMutableArray *offerFullList = [[NSMutableArray alloc] init];
            if(strSucces == true){
                NSArray *offerList = [json valueForKey:@"OfferList"];
                [offerList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
                    Offer *offer=[[Offer alloc]initWithDictionary:menuDict];
                    [offerFullList addObject:offer];
                    
                }];
                handler (offerFullList,nil);
            }else{
                handler (offerFullList,nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];
    
}
+(void)saveMyOffer:(NSString*)strOffer onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kSaveOffer];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
    
    
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/%@/%@",target,strOffer];
    //http://api.experiture.com/rest/v1/json/saveMyOfferRest/27CCC3A558E542DD827649EED27F64C3/2023/11
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            BOOL strSucces = [[json valueForKey:@"Success"]boolValue];
            if(strSucces == true){
                handler (@"1",nil);
            }else{
                handler (@"0",nil);
            }
            
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
+(void)scanOfferCode:(NSString *)strOffer onCompletion:(FetchCompletionHandler)handler{
    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kScanOffer];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *target = [prefs stringForKey:@"target"];
    
    //strOffer = @"POINTS4";
    NSString *kQuery = [NSString stringWithFormat:@"/27CCC3A558E542DD827649EED27F64C3/%@/%@",target,strOffer];
    //http://api.experiture.com/rest/v1/json/saveMyOfferRest/27CCC3A558E542DD827649EED27F64C3/2023/11
    
    SSRestManager *restManager = [[SSRestManager alloc] init];
    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
        if (json) {
            NSInteger succes = [[json valueForKey:@"Success"]integerValue];
            NSString *strSuccess = [NSString stringWithFormat:@"%ld",(long)succes];
           handler (strSuccess,nil);
        }
    } onError:^(NSError *error) {
        if (error) {
            handler(nil,error);
        }
    }];

}
//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}
//{"Success":true,"msg":"Login successfully","User_Details":{"Name":"Chandrahas","TargetId":"2022","UserPoints":5647,"Tire":"Platinum"}}
//+(void)fetchWovaxMenusOnCompletion:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl = [NSString stringWithFormat:@"%@%@",kApiKey,kMenuURL];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:menuUrl onCompletion:^(NSDictionary *json) {
//        if (json) {
//                NSArray *menuList = [json valueForKey:@"menu"];
//                NSMutableArray *menuFullList = [[NSMutableArray alloc] init];
//                [menuList enumerateObjectsUsingBlock:^(NSDictionary *menuDict, NSUInteger idx, BOOL *stop) {
//                    Menu *menu=[[Menu alloc]initWithDictionary:menuDict];
//                    [menuFullList addObject:menu];
//                    
//                }];
//                
//            handler (menuFullList,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)fetchImageDataWithLink:(NSString *)imageLink OnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    [restManager getServiceResponseWithBaseUrl:imageLink query:nil onCompletion:^(id responseData, NSURLResponse *reponse) {
//        handler (responseData,nil);
//    } onError:^(NSError *error) {
//        handler (nil,error);
//    }];
//
//}
//+ (void)fetchRecentPostOnCompletion:(FetchCompletionHandler)handler
//{
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSError *jsonError;
//        if (!jsonError) {
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                SSRestManager *restManager = [[SSRestManager alloc] init];
//                NSString *strPost = [NSString stringWithFormat:@"%@%@",kApiKey,kPostURL];
//                [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPost onCompletion:^(NSDictionary *json) {
//                    if (json) {
//                        NSArray *arrPost = [json valueForKey:@"posts"];
//                        NSMutableArray *postList = [[NSMutableArray alloc] init];
//                        [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                            Post *recent = [[Post alloc]initWithDictionary:dict];
//                            if(![recent.postHidden isEqualToString:@"1"] && ![recent.postCatId isEqualToString:@"3"]&& ![recent.postCatId isEqualToString:@"5"]){
//                                [postList addObject:recent];
//                            }
//                            
//                        }];
//                        handler (postList,nil);
//                    }
//                } onError:^(NSError *error) {
//                    if (error) {
//                        handler(nil,error);
//                    }
//                }];
//
//                
//                
//                
//            });
//        }
//    });
//
//    
//    
//}
//
//+ (void)fetchPostDetails:(NSString *)strPostId onCompletion:(FetchCompletionHandler)handler
//{
//    NSString *strPostDetails = [NSString stringWithFormat:@"%@%@",kApiKey,kPostDetails];
//    NSString *kQuery = [strPostDetails stringByAppendingString:strPostId];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSDictionary *dictPost = [json valueForKey:@"post"];
//           Post *recent = [[Post alloc]initWithDictionary:dictPost];
//            handler (recent,nil);
//          }
//       } onError:^(NSError *error) {
//           if (error) {
//               handler(nil,error);
//           }
//        
//    }];
//}
//
//+ (void)submitComment:(NSString *)strPostId  withText:(NSString*)strComment  onCompletion:(FetchCompletionHandler)handler {
//    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
//    NSString *name = [prefs stringForKey:@"Name"];
//    name = [name stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//    NSString *email = [prefs stringForKey:@"Email"];
//    NSString *website = [prefs stringForKey:@"Website"];
//    strComment = [self URLEncodeStringFromString:strComment];
//    NSString *strPost = [NSString stringWithFormat:@"post_id=%@&name=%@&url=%@&email=%@&content=%@", strPostId,name,website,email,strComment];
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@",kBaseURL,kApiKey,kComment,strPost];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:kQuery query:@"" onCompletion:^(NSDictionary *json) {
//        if(json){
//           handler (@"",nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (NSString *)URLEncodeStringFromString:(NSString *)string {
//    static CFStringRef charset = CFSTR("!@#$%&*()+'\";:=,/?[] ");
//    CFStringRef str = (__bridge CFStringRef)string;
//    CFStringEncoding encoding = kCFStringEncodingUTF8;
//    return (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, str, NULL, charset, encoding));
//}
//+ (void)fetchMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler{
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@&page=%ld",kBaseURL,kApiKey,kMenuDetails,strMenuId,(long)page];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSArray *arrPost = [json valueForKey:@"posts"];
//            NSMutableArray *postList = [[NSMutableArray alloc] init];
//            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                Post *recent = [[Post alloc]initWithDictionary:dict];
//                [postList addObject:recent];
//            }];
//            handler (postList,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//    
//}
//+ (void)fetchtagMenuDetails:(NSString*)strMenuId withPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler{
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@%@&page=%ld",kBaseURL,kApiKey,kTagDetails,strMenuId,(long)page];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSArray *arrPost = [json valueForKey:@"posts"];
//            NSMutableArray *postList = [[NSMutableArray alloc] init];
//            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                Post *recent = [[Post alloc]initWithDictionary:dict];
//                [postList addObject:recent];
//            }];
//            handler (postList,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//
//}
//+ (void)fetchSearchDetails:(NSString *)strSearch onCompletiion:(FetchCompletionHandler)handler
//{
//    strSearch = [strSearch stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@",kApiKey,kSearch,strSearch];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSArray *arrPost = [json valueForKey:@"posts"];
//            NSMutableArray *postList = [[NSMutableArray alloc] init];
//            [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                Post *recent = [[Post alloc]initWithDictionary:dict];
//                [postList addObject:recent];
//            }];
//            handler (postList,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//}
//+ (void)fetchPostTypeDetails:(NSString *)strPostId withType:(NSString*)strType onCompletion:(FetchCompletionHandler)handler {
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@post_id=%@&post_type=%@",kBaseURL,kApiKey,kPostDetailsType,strPostId,strType];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:kQuery query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSDictionary *dictPost = [json valueForKey:@"post"];
//            Post *recent = [[Post alloc]initWithDictionary:dictPost];
//            handler (recent,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//
//+ (void)fetchRecentPostPagewiseOnCompletion:(NSInteger)page onCompletion:(FetchCompletionHandler)handler {
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL ,kApiKey,kOptions];
//        NSData *trackerData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlString]];
//        NSError *jsonError;
//        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:trackerData options:kNilOptions error:&jsonError];
//        if (!jsonError) {
//            dispatch_async(dispatch_get_main_queue(), ^(void){
//                NSString *excludepost = [json objectForKey:@"wovax_excluded_categories"];
//                NSArray *arr;
//                if(![excludepost isEqualToString:@""]){
//                    NSString *leftString = [excludepost substringFromIndex:1];
//                    arr = [leftString componentsSeparatedByString:@","];
//                }
//                SSRestManager *restManager = [[SSRestManager alloc] init];
//                NSString *strPage = [NSString stringWithFormat:@"%@%@%ld",kApiKey,kPostURLPage,(long)page];
//                [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
//                    if (json) {
//                        NSArray *arrPost = [json valueForKey:@"posts"];
//                        NSMutableArray *postList = [[NSMutableArray alloc] init];
//                        [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                            Post *recent = [[Post alloc]initWithDictionary:dict];
//                            if(arr.count > 0){
//                                if(![recent.postHidden isEqualToString:@"1"] && ![arr containsObject:recent.postCatId]){
//                                    [postList addObject:recent];
//                                }
//                            }else{
//                                if(![recent.postHidden isEqualToString:@"1"]){
//                                    [postList addObject:recent];
//                                }
//                            }
//                            
//                        }];
//                        handler (postList,nil);
//                        
//                    }
//                } onError:^(NSError *error) {
//                    if (error) {
//                        handler(nil,error);
//                    }
//                }];
//            });
//        }else{
//            NSLog(@"%@",jsonError.debugDescription);
//        }
//    });
//    
//}
//+ (void)callClearBadgeTokenServiceOncompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *menuUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kClearBadgeToken];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:menuUrl onCompletion:^(NSDictionary *json) {
//        if (json) {
//            handler (nil,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)callMapService:(FetchCompletionHandler)handler{
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",kBaseURL,kApiKey,kMap];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:urlString onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSString *satuts = [json valueForKey:@"status"];
//            if([satuts isEqualToString:@"ok"]){
//                 NSArray *arrPost = [json valueForKey:@"posts"];
//                 NSMutableArray *arrMap = [[NSMutableArray alloc]init];
//                 [arrPost enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop) {
//                     Post *recent = [[Post alloc]init];
//                     recent.postTitle = [dict valueForKey:@"title"];
//                     recent.postLongitude = dict[@"custom_fields"][@"geo_longitude"];
//                     recent.postLatt = dict[@"custom_fields"][@"geo_latitude"];
//                     recent.postId = [dict valueForKey:@"id"];
//                     [arrMap addObject:recent];
//                 }];
//                 handler (arrMap,nil);
//            }
//            
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//
//}
//+ (void)storeDeviceMeataDataToBackendOnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *vendorID = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//    NSString *systemName = [[UIDevice currentDevice] systemName];
//    NSString *systemModel = [[UIDevice currentDevice] model];
//    NSString *menuUrl = [NSString stringWithFormat:@"/?wovaxmenu=downloads&userID=%@&OS=%@&device=%@",vendorID,systemName,systemModel];
//    NSString *finalURL  = [menuUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"]
//    ;    [restManager getJsonResponseFromBaseUrl:kBaseURL query:finalURL onCompletion:^(NSDictionary *json) {
//        if (json) {
//            handler (nil,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)getAppSettingsDataOnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *urlString = [NSString stringWithFormat:@"/%@%@" ,kApiKey,kOptions2];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:urlString onCompletion:^(NSDictionary *json) {
//        if (json) {
//            if([[json valueForKey:@"status"] isEqualToString:@"ok"]){
//                AppSettings *appSettings = [[AppSettings alloc] initWithDict:json];
//                handler(appSettings,nil);
//            }
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)getWovaxSettingsDataOnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//     NSString *urlString = [NSString stringWithFormat:@"/%@%@" ,kApiKey,kOptions];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:urlString onCompletion:^(NSDictionary *json) {
//        if (json) {
//            WovaxSetting *wovaxSettings = [[WovaxSetting alloc] initWithDict:json];
//            handler(wovaxSettings,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//+ (void)fetchRecentPostDictOfPage:(NSInteger)page onCompletion:(FetchCompletionHandler)handler {
//    /*NSString *query = [NSString stringWithFormat:@"%@%@%ld",kApiKey,kPostURLPage,(long)page];
//    NSString *url = [NSString stringWithFormat:@"%@%@",kBaseURL,query];
//    NSDictionary *headerDict;
//    SSHTTPClient *client = [[SSHTTPClient alloc] initWithUrl:url method:@"GET" httpBody:@"" headerFieldsAndValues:headerDict];
//    [client getJsonData:^(id jsonData, NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }else if (jsonData) {
//            handler(jsonData,nil);
//        }else {
//            handler(nil,nil);
//        }
//    }];*/
//    
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *strPage = [NSString stringWithFormat:@"%@%@%ld",kApiKey,kPostURLPage,(long)page];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
//        if (json) {
//            handler (json,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//#pragma mark Notification Service
//+ (void)fetchNotificationOfDeviceId:(NSString *)deviceId OnCompletion:(FetchCompletionHandler)handler {
//    SSRestManager *restManager = [[SSRestManager alloc] init];
//    NSString *strPage = [NSString stringWithFormat:@"%@%@?deviceId=%@&platform=apns",kApiKey,kNotificationURL,deviceId];
//    [restManager getJsonResponseFromBaseUrl:kBaseURL query:strPage onCompletion:^(NSDictionary *json) {
//        if (json) {
//            NSArray *categories = json[@"categories"];
//            NSMutableArray *notiifcationList = [[NSMutableArray alloc] init];
//            [categories enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
//                Notification *notif = [[Notification alloc] initWithDict:obj];
//                [notiifcationList addObject:notif];
//            }];
//            handler (notiifcationList,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//    }];
//}
//#pragma mark Push notification
//+ (void)registerRemoteNotificationWithDeviceId:(NSString *)deviceToken onCompletion:(FetchCompletionHandler)handler {
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@",kApiKey,kRegisterAPNS,deviceToken];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            handler (json,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//}
//+ (void)unsubscribePushNotificationOnCatergories:(NSString *)categories deviceId:(NSString *)deviceId onCompletion:(FetchCompletionHandler)handler {
//    NSString *kQuery = [NSString stringWithFormat:@"%@%@%@&categories=%@",kApiKey,kUnsubscribeAPNS,deviceId,categories];
//    SSRestManager *restmanger = [[SSRestManager alloc]init];
//    [restmanger getJsonResponseFromBaseUrl:kBaseURL query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            handler (json,nil);
//        }
//    } onError:^(NSError *error) {
//        handler(nil,error);
//    }];
//}
@end
