//
//  DatepickerView.m
//  GoldCasino
//
//  Created by MAPPS MAC on 18/04/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "DatepickerView.h"

@interface DatepickerView ()
- (IBAction)btnCloseAction:(UIButton *)sender;

- (IBAction)valueChange:(UIDatePicker *)sender;
- (IBAction)btnCancelAction:(UIButton *)sender;

@end

@implementation DatepickerView

- (void)viewDidLoad {
    [super viewDidLoad];
//    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
//    NSDate *currentDate = [NSDate date];
//    NSDateComponents *comps = [[NSDateComponents alloc] init];
//    [comps setYear:16];
//    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
//    [comps setYear:-18];
//    
//    [self.datepicker setMaximumDate:maxDate];
        // Do any additional setup after loading the view.
}
- (void)setEventOnCompletionDe:(EventCompletionHandlerPicker)handler{
    eventCompletionHandlerPic = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCloseAction:(UIButton *)sender {
    eventCompletionHandlerPic(nil,kPicClose);
}
- (IBAction)valueChange:(UIDatePicker *)sender {
    eventCompletionHandlerPic(nil,kPicOpen);
}
- (IBAction)btnCancelAction:(UIButton *)sender {
    eventCompletionHandlerPic(nil,kPicCancel);
}
@end
