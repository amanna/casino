//
//  EventVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeE {
    kEventTapMenu = 0,
    kEventCasino = 1,
    kEventDining = 2,
    kEventEvent = 3,
    kEventHotel = 4,
    kEventNight = 5,
    kEventTap=6,
    kEventDetails=7,
    kEventNotify=8
};
typedef void (^EventCompletionHandlerE)(id object, NSUInteger EventTypeE);
@interface EventVC : UIViewController{
    EventCompletionHandlerE eventCompletionHandlerE;
}
- (void)setEventOnCompletion:(EventCompletionHandlerE)handler ;
@property (weak, nonatomic) IBOutlet UIView *viewTrans;
@property(nonatomic)NSString *strSectionId;
@end
