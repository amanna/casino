//
//  HomeVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeH {
    kActionTapMenu = 0,
    kActionCasino = 1,
    kActionDining = 2,
    kActionEvent = 3,
    kActionHotel = 4,
    kActionNight = 5,
    kActionSocial = 6,
    kActionAccount = 7,
    kActionNotify = 8,
    kActionTap=9,
    kActionOffer=10,
    kScan=11
};
typedef void (^EventCompletionHandlerH)(id object, NSUInteger EventTypeH);
@interface HomeVC : UIViewController{
     EventCompletionHandlerH eventCompletionHandlerH;
}
- (void)setEventOnCompletion:(EventCompletionHandlerH)handler ;
- (IBAction)btnCasinoAction:(UIButton *)sender;
- (IBAction)btnDiningAction:(UIButton *)sender;

- (IBAction)btnHotelAction:(UIButton *)sender;
- (IBAction)btnMediaAction:(UIButton *)sender;
- (IBAction)btnEventAction:(UIButton *)sender;
- (IBAction)btnNightAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *transView;


@property (weak, nonatomic) IBOutlet UILabel *lblWelcome;
@property (weak, nonatomic) IBOutlet UILabel *lblAnAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@property (weak, nonatomic) IBOutlet UIView *viewAftrLogin;



//After Login
- (IBAction)btnOfferAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblUserNmae;
@property (weak, nonatomic) IBOutlet UILabel *lblTier;
@property (weak, nonatomic) IBOutlet UILabel *lblPoint;
@end
