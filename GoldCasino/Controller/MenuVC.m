//
//  MenuVC.m
//  GoldCasino
//
//  Created by MAPPS MAC on 15/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "MenuVC.h"
#import "MenuCell.h"
#import "AppDelegate.h"
#import "AboutVC.h"

@interface MenuVC (){
    AppDelegate *delegate;
}
@property (weak, nonatomic) IBOutlet UITableView *myTbl;
@property(nonatomic)NSMutableArray *arrContent;
@property(nonatomic)NSMutableArray *arrContentImg;

@end

@implementation MenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Add an observer that will respond to loginComplete
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showLatest:)
                                                 name:@"loginComplete" object:nil];


    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUname = [prefs stringForKey:@"username"];
    if(strUname==nil){
        self.arrContent = [[NSMutableArray alloc]initWithObjects:@"Home",@"Offers",@"Casino", @"Dining",@"Events",@"Nightlife",@"Hotel",@"About Us",@"Social Media",@"Settings",@"Login",nil];

    }else{
       self.arrContent = [[NSMutableArray alloc]initWithObjects:@"Home",@"Offers",@"Casino", @"Dining",@"Events",@"Nightlife",@"Hotel",@"About Us",@"Social Media",@"Settings",@"Logout",nil];
    }
    
    
     self.arrContentImg = [[NSMutableArray alloc]initWithObjects:@"menu-home.png",@"menu-offer.png",@"menu-casino.png", @"menu-dining.png",@"menu-events.png",@"menu-nightlife.png",@"menu-hotel.png",@"menu-about.png",@"menu-social.png",@"menu-setting.png",@"menu-logout.png",nil];
//    UITapGestureRecognizer *tapRec = [[UITapGestureRecognizer alloc]
//                                      initWithTarget:self action:@selector(tap:)];
//    [tapRec setCancelsTouchesInView:NO];
//    [self.view addGestureRecognizer:tapRec];
//    
     self.myTbl.separatorColor = [UIColor clearColor];
    // Do any additional setup after loading the view.
}
- (void)showLatest:(NSNotification *)note {
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUname = [prefs stringForKey:@"username"];
    if(strUname==nil){
        self.arrContent = [[NSMutableArray alloc]initWithObjects:@"Home",@"Offers",@"Casino", @"Dining",@"Events",@"Nightlife",@"Hotel",@"About Us",@"Social Media",@"Settings",@"Login",nil];
        
    }else{
        self.arrContent = [[NSMutableArray alloc]initWithObjects:@"Home",@"Offers",@"Casino", @"Dining",@"Events",@"Nightlife",@"Hotel",@"About Us",@"Social Media",@"Settings",@"Logout",nil];
    }
    [self.myTbl reloadData];
}
- (void)tap:(UITapGestureRecognizer*)rec{
   // [delegate disAppearMenu];
}
- (void)setEventOnCompletion:(EventCompletionHandler)handler {
    eventCompletionHandler = handler;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrContent.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
  MenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
  if(!cell){
      [tableView registerNib:[UINib nibWithNibName:@"MenuCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
      cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
  }
    cell.lblMenu.text = [self.arrContent objectAtIndex:indexPath.row];
    cell.leftImg.image = [UIImage imageNamed:[self.arrContentImg objectAtIndex:indexPath.row]];
    if(indexPath.row== self.arrContent.count - 1){
        cell.lblSep.hidden = YES;
    }else if (indexPath.row== self.arrContent.count - 2){
        cell.lblSep.hidden = YES;
    }else if (indexPath.row== self.arrContent.count - 3){
        cell.lblSep.hidden = YES;
    }
    else{
        cell.lblSep.hidden = NO;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
  return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *str = [NSString stringWithFormat:@"%ld",(long)indexPath.row];
    eventCompletionHandler(str,kActionSelect);
    //[delegate disAppearMenu:indexPath.row withVc:self];
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 62;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
