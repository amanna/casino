//
//  DiningVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 14/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeD {
    kDinTapMenu = 0,
    kDinCasino = 1,
    kDinDining = 2,
    kDinEvent = 3,
    kDinHotel = 4,
    kDinNight = 5,
    kDinTap=6,
    kDinDetails=7,
    kNotifyD
};
typedef void (^EventCompletionHandlerD)(id object, NSUInteger EventTypeD);

@interface DiningVC : UIViewController{
     EventCompletionHandlerD eventCompletionHandlerD;
}
- (void)setEventOnCompletion:(EventCompletionHandlerD)handler ;
@property (weak, nonatomic) IBOutlet UIView *viewTrans;
@property(nonatomic)NSString *strSectionId;
@end
