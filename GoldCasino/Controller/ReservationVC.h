//
//  ReservationVC.h
//  GoldCasino
//
//  Created by Amrita on 30/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeRe {
    kReMenu = 0,
    kReBack = 1,
    kReTap=2
};
typedef void (^EventCompletionHandlerRe)(id object, NSUInteger EventTypeRe);

@interface ReservationVC : UIViewController{
     EventCompletionHandlerRe eventCompletionHandlerRe;
}
@property (weak, nonatomic) IBOutlet UILabel *lblNav;
- (void)setEventOnCompletionRe:(EventCompletionHandlerRe)handler;
@end
