//
//  SocialVC.h
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeS {
    kSocialMenu = 0,
    kSocialTap = 1,
    kSocialNotify=2
};
typedef void (^EventCompletionHandlerS)(id object, NSUInteger EventTypeS);
@interface SocialVC : UIViewController{
    EventCompletionHandlerS eventCompletionHandlerS;
}
- (void)setEventOnCompletionS:(EventCompletionHandlerS)handler ;

@property (weak, nonatomic) IBOutlet UIView *viewTrans;

@end
