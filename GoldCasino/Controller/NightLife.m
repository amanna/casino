//
//  NightLife.m
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "NightLife.h"
#import "GoldCell.h"
#import "GoldCellHeader.h"
#import "HeaderCell.h"
#import "AppDelegate.h"
#import "Night.h"
#import "POAcvityView.h"
#import "WebServiceManager.h"
#import "Section.h"
#import "Pages.h"

@interface NightLife ()<UITableViewDataSource,UITableViewDelegate>{
    AppDelegate *delegate;
     POAcvityView *activity;
}
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property(nonatomic)NSMutableArray *arrNight;
@property(nonatomic)BOOL isAllOff1;
@property(nonatomic)BOOL isAllOff2;
@end

@implementation NightLife

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isAllOff1 = NO;
    self.isAllOff2 = NO;
     activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    self.arrNight = [[NSMutableArray alloc]init];
    UITapGestureRecognizer *singleFingerTap =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(handleSingleTap:)];
    [singleFingerTap setCancelsTouchesInView:NO];
    [self.view addGestureRecognizer:singleFingerTap];
    delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.tblView.separatorColor = [UIColor clearColor];
    CGFloat tableHeight = 3 * 102.0f+ 1*52;
    [self.tblView setContentSize:CGSizeMake(self.tblView.contentSize.width, tableHeight)];

    // Do any additional setup after loading the view.
}
- (void)handleSingleTap:(UITapGestureRecognizer *)recognizer {
   if(self.view.frame.origin.x==0){
        self.tblView.allowsSelection = YES;
         eventCompletionHandlerN(@"1",kNightTap);
    }else{
        self.tblView.allowsSelection = NO;
         eventCompletionHandlerN(@"1",kNightTap);
    }
}
- (void)setEventOnCompletion:(EventCompletionHandlerN)handler{
    self.isAllOff1 = NO;
    self.isAllOff2 = NO;
   [activity showView];
    Section *section = [delegate.arrSection objectAtIndex:4];
    self.strSectionId = section.strSectionId;
    [WebServiceManager getPageListOnCompletion:self.strSectionId onCompletion:^(id object, NSError *error) {
        self.isAllOff1= YES;
        if(self.isAllOff2==YES && self.isAllOff1==YES){
            [activity hideView];
        }
        if(object){
             eventCompletionHandlerN = handler;
            self.arrNight = object;
            [self.tblView reloadData];
        }else{
            
        }
        
    }];

}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.arrNight.count;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    // 1. Dequeue the custom heypeader cell
    HeaderCell* cell = [tableView dequeueReusableCellWithIdentifier:@"hCell"];
    if(!cell){
        [tableView registerNib:[UINib nibWithNibName:@"MenuHead" bundle:nil] forCellReuseIdentifier:@"hCell"];
        cell = [tableView dequeueReusableCellWithIdentifier:@"hCell"];
    }
    //cell.imgBanner.image= [UIImage imageNamed:@"bannern.png"];
    Section *sec = [delegate.arrSection objectAtIndex:4];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^(void) {
        
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:sec.strSectionMain]];
        
        UIImage* image = [[UIImage alloc] initWithData:imageData];
        if (image) {
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imgBanner.image = image;
                self.isAllOff2 = YES;
                if(self.isAllOff2==YES && self.isAllOff1==YES){
                    [activity hideView];
                }
            });
        }
    });

   // bannern
    [cell.btnMenu addTarget:self action:@selector(btnMenuAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnCasino addTarget:self action:@selector(btnCasinoAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnDining addTarget:self action:@selector(btnDiningAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnEvent addTarget:self action:@selector(btnEventAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnHotel addTarget:self action:@selector(btnHotelAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnNight addTarget:self action:@selector(btnNightAction) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnNotice addTarget:self action:@selector(btnNoticeAction) forControlEvents:UIControlEventTouchUpInside];

    return cell;
}
- (void)btnNoticeAction{
     eventCompletionHandlerN(nil,kNightNotify);
}
- (void)btnMenuAction{
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerN(@"0",kNightTapMenu);
    }else{
        eventCompletionHandlerN(@"1",kNightTapMenu);
    }
}
- (void)btnCasinoAction{
    eventCompletionHandlerN(nil,kNightCasino);
}
- (void)btnDiningAction{
    eventCompletionHandlerN(nil,kNightDining);
    
}
- (void)btnEventAction{
    eventCompletionHandlerN(nil,kNightEvent);
}
- (void)btnNightAction{
    eventCompletionHandlerN(nil,kNightNight);
}
- (void)btnHotelAction{
    eventCompletionHandlerN(nil,kNightHotel);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 241 + 59 - 42;
}
// Row display. Implementers should *always* try to reuse cells by setting each cell's reuseIdentifier and querying for available reusable cells with dequeueReusableCellWithIdentifier:
// Cell gets various attributes set automatically based on table (separators) and data source (accessory views, editing controls)

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Pages *page = [self.arrNight objectAtIndex:indexPath.row];
    if([page.strPageType isEqualToString:@"header"]){
        GoldCellHeader *cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCellHeader" bundle:nil] forCellReuseIdentifier:@"CellH"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"CellH"];
        }
        cell.lblContent.text = page.strSubTitle;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else{
        GoldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if(!cell){
            [tableView registerNib:[UINib nibWithNibName:@"GoldCell" bundle:nil] forCellReuseIdentifier:@"Cell"];
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        }
        cell.tag = indexPath.row;
        cell.lblTitle.text = page.strPageTitle;
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:
                                               [page.strPageDesc dataUsingEncoding:NSUnicodeStringEncoding] options:
                                               @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                                           documentAttributes:nil error:nil];
        
        
        
        
        [attrStr enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrStr.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            
            UIFont* font = value;
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
            // font = [font fontWithSize:14.0];
            
            [attrStr removeAttribute:NSFontAttributeName range:range];
            [attrStr addAttribute:NSFontAttributeName value:font range:range];
        }];
        
         [attrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, attrStr.length)];
        cell.lblDesc.attributedText = attrStr;
       
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            NSString *strImage = [NSString stringWithFormat:@"https://landingpage.experiture.com/TrackingSystem/AG_44625/AD_44626/Images/%@",page.strPageThumbImg];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:strImage]];
            
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (cell.tag == indexPath.row) {
                        cell.imgGallery.image = image;
                        [cell setNeedsLayout];
                    }
                });
            }
        });

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Pages *page = [self.arrNight objectAtIndex:indexPath.row];
    if([page.strPageType isEqualToString:@"header"]){
        
    }else{
        delegate.page = page;
         eventCompletionHandlerN(page,kNightDetails);
    }

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    Pages *page = [self.arrNight objectAtIndex:indexPath.row];
    if([page.strPageType isEqualToString:@"header"]){
        return 52;
    }else{
        return 102;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
