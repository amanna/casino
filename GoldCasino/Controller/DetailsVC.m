//
//  DetailsVC.m
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "DetailsVC.h"
#import "AppDelegate.h"
@interface DetailsVC (){
    AppDelegate *delegate;
}
- (IBAction)btnBackAction:(UIButton *)sender;
- (IBAction)btnMenuAction:(UIButton *)sender;
@end

@implementation DetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
     delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    self.lblNav.text= delegate.strDetailsTitle;
    
   // self.myText.font = [UIFont systemFontOfSize:17.0];
    
    
        // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setEventOnCompletionDe:(EventCompletionHandlerDe)handler{
    eventCompletionHandlerDe = handler;
}
- (IBAction)btnBackAction:(UIButton *)sender {
    eventCompletionHandlerDe(nil,kDeBack);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnMenuAction:(UIButton *)sender {
    if(self.view.frame.origin.x==0){
        eventCompletionHandlerDe(@"0",kDeMenu);
    }else{
        eventCompletionHandlerDe(@"1",kDeMenu);
    }
}
@end
