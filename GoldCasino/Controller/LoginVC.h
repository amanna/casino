//
//  LoginVC.h
//  GoldCasino
//
//  Created by MAPPS MAC on 25/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeL {
    kLoginAction = 0
};
typedef void (^EventCompletionHandlerL)(id object, NSUInteger EventTypeL);
@interface LoginVC : UIViewController{
    EventCompletionHandlerL eventCompletionHandlerL;
}
- (void)setEventOnCompletion:(EventCompletionHandlerL)handler ;
@end
