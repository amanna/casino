//
//  DetailsVC.h
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeDe {
    kDeMenu = 0,
    kDeBack = 1,
    kDeTap=2
};
typedef void (^EventCompletionHandlerDe)(id object, NSUInteger EventTypeDe);

@interface DetailsVC : UIViewController{
    EventCompletionHandlerDe eventCompletionHandlerDe;
}
- (void)setEventOnCompletionDe:(EventCompletionHandlerDe)handler;
@property (weak, nonatomic) IBOutlet UITextView *myText;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNav;
@end
