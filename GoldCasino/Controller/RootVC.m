//
//  RootVC.m
//  GoldCasino
//
//  Created by Amrita on 25/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "RootVC.h"
#import "HomeVC.h"
#import "CasinoVC.h"
#import "DiningVC.h"
#import "HotelVC.h"
#import "EventVC.h"
#import "OfferVC.h"
#import "MenuVC.h"
#import "AboutVC.h"
#import "SocialVC.h"
#import "SettingsVC.h"
#import "NightLife.h"
#import "AppDelegate.h"
#import "DetailsVC.h"
#import "LoginVC.h"
#import "ReservationVC.h"
#import "ScannerVC.h"
//#import "WinnerVC.h"
#import <CoreLocation/CoreLocation.h>
#import "WebServiceManager.h"
#import "NotificationVC.h"
#import "POAcvityView.h"
#import <QuartzCore/QuartzCore.h>
#import "Section.h"

@interface RootVC ()<CLLocationManagerDelegate>{
    AppDelegate *delegate;
    CLLocationManager *locationManager;
    POAcvityView *activity;
}
- (IBAction)selectedTab:(id)sender;
@property(nonatomic)HomeVC  *homevc;
@property(nonatomic)MenuVC  *menuvc;
@property(nonatomic)CasinoVC  *casinovc;
@property(nonatomic)DiningVC  *diningvc;
@property(nonatomic)HotelVC  *hotelvc;
@property(nonatomic)EventVC  *eventvc;
@property(nonatomic)OfferVC  *offervc;

@property(nonatomic)SocialVC  *socialVc;
@property(nonatomic)SettingsVC  *settingsvc;
@property(nonatomic)NightLife  *nightvc;
@property(nonatomic)AboutVC  *aboutVc;
@property(nonatomic)DetailsVC  *detailsVc;
@property(nonatomic)LoginVC  *loginVc;

@property(nonatomic)ScannerVC  *scannerVC;

@property(nonatomic)NotificationVC  *notifyVC;
@property(nonatomic)ReservationVC  *reserveVc;
@property(nonatomic)BOOL isFlag;
@end

@implementation RootVC
- (void)setEventOnCompletionR:(EventCompletionHandlerR)handler {
    eventCompletionHandlerR = handler;
}
-(void)addHome{
    [self.view addSubview:self.homevc.view];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    NSLog(@"%@", [locations lastObject]);
    CLLocation *newLocation = [locations lastObject];
    NSLog(@"%f ------- %f",newLocation.coordinate.latitude,newLocation.coordinate.longitude);
    NSString *strLatt = [[NSNumber numberWithFloat:newLocation.coordinate.latitude] stringValue];
    
     NSString *strLongi = [[NSNumber numberWithFloat:newLocation.coordinate.longitude] stringValue];
    

      if(self.isFlag==true){
          self.isFlag = false;
          [WebServiceManager saveMyLatLongitude:strLatt andLoni:strLongi onCompletion:^(id object, NSError *error) {
              if(object){
                  self.isFlag = true;
                  NSLog(@"save location");
              }else{
                  NSLog(@"not saving location");
              }
          }];
      }


     //[manager stopUpdatingLocation];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIStoryboard *storyboard;
    
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        //its iPhone. Find out which one?
        
        CGSize result = [[UIScreen mainScreen] bounds].size;
        if(result.height == 480)
        {
            storyboard =
            [UIStoryboard storyboardWithName:@"Main1"
                                      bundle:nil];
            
        }
        else if(result.height == 568)
        {
            storyboard =
            [UIStoryboard storyboardWithName:@"Main1"
                                      bundle:nil];
                   }
        else {
            storyboard =
            [UIStoryboard storyboardWithName:@"Main"
                                      bundle:nil];
            
        }
    }
    
    delegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    activity = [[POAcvityView alloc]initWithTitle:@"Loading.." message:@"Loading.."];
    self.isFlag=true;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLLocationAccuracyBest;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
     [locationManager requestAlwaysAuthorization]; //Note this one
    [locationManager startUpdatingLocation];
    
   
    
    
    self.homevc =
    [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
   self.casinovc =
   [storyboard instantiateViewControllerWithIdentifier:@"CasinoVC"];
  
   self.diningvc =
   [storyboard instantiateViewControllerWithIdentifier:@"DiningVC"];
   self.hotelvc =
   [storyboard instantiateViewControllerWithIdentifier:@"HotelVC"];
   self.eventvc =
   [storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
   self.offervc =
   [storyboard instantiateViewControllerWithIdentifier:@"OfferVC"];
   
   
   self.socialVc =
   [storyboard instantiateViewControllerWithIdentifier:@"SocialVC"];
   self.settingsvc =
   [storyboard instantiateViewControllerWithIdentifier:@"SettingsVC"];
   self.nightvc =
   [storyboard instantiateViewControllerWithIdentifier:@"NightLife"];
   self.aboutVc =
   [storyboard instantiateViewControllerWithIdentifier:@"AboutVC"];
   
   self.menuvc =
   [storyboard instantiateViewControllerWithIdentifier:@"MenuVC"];
   
   self.notifyVC =
   [storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];

   self.detailsVc =
   [storyboard instantiateViewControllerWithIdentifier:@"DetailsVC"];
   
   self.loginVc =
   [storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
   
   self.reserveVc =
   [storyboard instantiateViewControllerWithIdentifier:@"ReservationVC"];
   self.scannerVC =
   [storyboard instantiateViewControllerWithIdentifier:@"ScannerVC"];


 
 CGRect frame2 = self.menuvc.view.frame;
 frame2.origin.x = - 400;
 self.menuvc.view.frame = frame2;
 [self.view addSubview:self.menuvc.view];
 [self.menuvc setEventOnCompletion:^(id object, NSUInteger eventType) {
     NSString *indexMenu = object;
     if(eventType==kActionSelect){
         [self closeMenuAction:indexMenu];
     }
 }];
 //Set Frame
    CGRect frame = self.homevc.view.frame;
    frame.size.height = (self.view.frame.size.height - 86);
    self.homevc.view.frame = frame;
    self.casinovc.view.frame = frame;
    self.diningvc.view.frame = frame;
    self.eventvc.view.frame = frame;
    self.hotelvc.view.frame = frame;
    self.offervc.view.frame = frame;
    self.settingsvc.view.frame = frame;
    self.socialVc.view.frame = frame;
    self.aboutVc.view.frame = frame;
    self.nightvc.view.frame = frame;
    self.detailsVc.view.frame = frame;
    self.reserveVc.view.frame= frame;
   
    self.scannerVC.view.frame= frame;
    self.notifyVC.view.frame= frame;
    
    [self.notifyVC setEventOnCompletion:^(id object, NSUInteger EventTypeN) {
        if(EventTypeN==kNotDetails){
            
        }else if (EventTypeN==kNotBack){
            
            CGRect frm = self.notifyVC.view.frame;
            frm.origin.x = 0;
            
            [UIView animateWithDuration:0.8 animations:^{
                CGRect frm1 = self.notifyVC.view.frame;
                frm1.origin.x = 500;
                self.notifyVC.view.frame=frm1;
                
            } completion:^(BOOL finished) {
                [self.notifyVC.view removeFromSuperview];
            }];

            
            //[self.notifyVC.view removeFromSuperview];
        }
    }];
    
    
    [self.scannerVC setEventOnCompletionSC:^(id object, NSUInteger EventScan) {
        if(EventScan==kScanBack){
           
            NSString *str = object;
            // [self.reserveVc.view removeFromSuperview];
            
            CGRect frm = self.scannerVC.view.frame;
            frm.origin.x = 0;
            
            [UIView animateWithDuration:0.8 animations:^{
                CGRect frm1 = self.reserveVc.view.frame;
                frm1.origin.x = 500;
                self.scannerVC.view.frame=frm1;
                
            } completion:^(BOOL finished) {
                 [self.scannerVC.view removeFromSuperview];
            }];
            

            
        }
    }];
    
    [self.reserveVc setEventOnCompletionRe:^(id object, NSUInteger EventTypeRe) {
        if(EventTypeRe==kReBack){
            NSString *str = object;
           // [self.reserveVc.view removeFromSuperview];
            
            CGRect frm = self.reserveVc.view.frame;
            frm.origin.x = 0;
            
            [UIView animateWithDuration:0.8 animations:^{
                CGRect frm1 = self.reserveVc.view.frame;
                frm1.origin.x = 500;
                self.reserveVc.view.frame=frm1;
                
            } completion:^(BOOL finished) {
                [self.reserveVc.view removeFromSuperview];
            }];

        }
        
    }];
    
    //Details Action
    
    [self.detailsVc setEventOnCompletionDe:^(id object, NSUInteger EventTypeDe) {
         NSString *str = object;
        if(EventTypeDe==kDeMenu){
            [self openCloseMenu:str withView:self.detailsVc.view];
        }else if(EventTypeDe==kDeTap){
            [self openCloseMenu:str withView:self.detailsVc.view];
        }else if(EventTypeDe==kDeBack){
            CGRect frm = self.detailsVc.view.frame;
            frm.origin.x = 0;
            
            [UIView animateWithDuration:0.8 animations:^{
                CGRect frm1 = self.detailsVc.view.frame;
                frm1.origin.x = 500;
                self.detailsVc.view.frame=frm1;

            } completion:^(BOOL finished) {
                 [self.detailsVc.view removeFromSuperview];
            }];
            
            

        }
    }];
   
    [self homeAction];
   
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    
    // getting an NSString
    NSString *strUname = [prefs stringForKey:@"username"];
    if(strUname==nil){
        self.homevc.viewAftrLogin.hidden = true;
        self.homevc.lblWelcome.hidden = false;
        self.homevc.lblAnAccount.hidden = false;
        self.homevc.btnLogin.hidden = false;
    }else{
        self.homevc.viewAftrLogin.hidden = false;
        self.homevc.lblWelcome.hidden = true;
        self.homevc.lblAnAccount.hidden = true;
        self.homevc.btnLogin.hidden = true;
        
        self.homevc.lblUserNmae.text = strUname;
      
        NSString *strUserPoint = [prefs stringForKey:@"userpoints"];
        NSString *strTire = [prefs stringForKey:@"Tire"];
        self.homevc.lblTier.text = [NSString stringWithFormat:@"Tier: %@",strTire];
        self.homevc.lblPoint.text =[NSString stringWithFormat:@"Points: %@",strUserPoint];
        
}
    
    [self.loginVc setEventOnCompletion:^(id object, NSUInteger EventTypeL) {
        if(EventTypeL== kLoginAction){
             NSString *strUname = [prefs stringForKey:@"username"];
            self.homevc.lblWelcome.hidden = true;
            self.homevc.lblAnAccount.hidden = true;
            self.homevc.btnLogin.hidden = true;
            
            self.homevc.viewAftrLogin.hidden = false;
            self.homevc.lblUserNmae.text = strUname;
          
            NSString *strUserPoint = [prefs stringForKey:@"userpoints"];
            NSString *strTire = [prefs stringForKey:@"Tire"];
            
            self.homevc.lblTier.text = [NSString stringWithFormat:@"Tier: %@",strTire];
            self.homevc.lblPoint.text =[NSString stringWithFormat:@"Points: %@",strUserPoint];
            

        }
    }];
    

    // Do any additional setup after loading the view.
}
- (void)loginAction{
    [self presentViewController:self.loginVc animated:YES completion:nil];
}
- (void)reserveAction{
    self.reserveVc.lblNav.text= delegate.strDetailsTitle;
    [self.viewTab.layer removeAllAnimations];
    CGRect frm = self.reserveVc.view.frame;
    frm.origin.x = 500;
    self.reserveVc.view.frame=frm;
    self.reserveVc.view.alpha = 0.0;
    [UIView animateWithDuration:0.8 animations:^{
        CGRect frm1 = self.reserveVc.view.frame;
        frm1.origin.x = 0;
        self.reserveVc.view.frame=frm1;
        self.reserveVc.view.alpha = 1.0;
        
        [self.view addSubview:self.reserveVc.view];
        
        
        
    }];
    

}
- (void)closeMenuAction:(NSString*)indexMenu{
    UIView *customView;
    if(self.homevc.view.frame.origin.x!=0){
        customView = self.homevc.view;
        self.homevc.transView.hidden= YES;
    }else if (self.casinovc.view.frame.origin.x!=0){
        customView = self.casinovc.view;
         self.casinovc.viewTrans.hidden= YES;
    }else if (self.diningvc.view.frame.origin.x!=0){
        customView = self.diningvc.view;
         self.diningvc.viewTrans.hidden= YES;
    }else if (self.hotelvc.view.frame.origin.x!=0){
        customView = self.hotelvc.view;
         self.hotelvc.viewTrans.hidden= YES;
    }else if (self.eventvc.view.frame.origin.x!=0){
        customView = self.eventvc.view;
         self.eventvc.viewTrans.hidden= YES;
    }else if (self.offervc.view.frame.origin.x!=0){
        customView = self.offervc.view;
         self.offervc.viewTrans.hidden= YES;
    }else if (self.nightvc.view.frame.origin.x!=0){
        customView = self.nightvc.view;
        self.nightvc.viewTrans.hidden= YES;
    }else if (self.aboutVc.view.frame.origin.x!=0){
        customView = self.aboutVc.view;
        self.aboutVc.viewTrans.hidden= YES;
    }else if (self.socialVc.view.frame.origin.x!=0){
        customView = self.socialVc.view;
         self.socialVc.viewTrans.hidden= YES;
    }else if (self.settingsvc.view.frame.origin.x!=0){
        customView = self.settingsvc.view;
         self.settingsvc.viewTrans.hidden= YES;
    }
    
    
    [UIView animateWithDuration:0.4 animations:^{
        CGRect frm = self.viewTab.frame;
        CGFloat fltWidth = ((self.view.frame.size.width * 80)/100);
        frm.origin.x = 0;
        self.viewTab.frame = frm;
        self.viewTab.userInteractionEnabled=YES;
        
        CGRect frm2 = customView.frame;
        frm2.origin.x = 0;
        customView.frame = frm2;
        
        CGRect frm3 = self.menuvc.view.frame;
        frm3.origin.x = -fltWidth;
        self.menuvc.view.frame = frm3;
    }completion:^(BOOL finished) {
       
        if([indexMenu isEqualToString:@"0"]){
            [self homeAction];
        }else if ([indexMenu isEqualToString:@"1"]){
           //[self offerAction];
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUname = [prefs stringForKey:@"username"];
            if(strUname==nil){
                [self loginAction];
            }else{
                [self offerAction];
            }

        }else if ([indexMenu isEqualToString:@"2"]){
            [self casinoAction];
        }else if ([indexMenu isEqualToString:@"3"]){
            [self diningAction];
        }else if ([indexMenu isEqualToString:@"4"]){
             [self eventAction];
        }else if ([indexMenu isEqualToString:@"5"]){
            [self nightAction];
        }else if ([indexMenu isEqualToString:@"6"]){
            [self hotelAction];
        }else if ([indexMenu isEqualToString:@"7"]){
             [self aboutAction];
        }else if ([indexMenu isEqualToString:@"8"]){
            [self socialAction];
        }else if ([indexMenu isEqualToString:@"9"]){
            NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
            NSString *strUname = [prefs stringForKey:@"username"];
            if(strUname==nil){
                [self loginAction];
            }else{
                [self settingsAction];
            }
            
        }else if ([indexMenu isEqualToString:@"10"]){
            [self logOutAction];
        }
    }];
}
- (void)logOutAction{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSString *strUname = [prefs stringForKey:@"username"];
    if(strUname==nil){
        [self loginAction];
    }else{
        NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
        NSString *strUname = [prefs stringForKey:@"username"];
        strUname = nil;
        [[NSUserDefaults standardUserDefaults] setObject:strUname forKey:@"username"];
        self.homevc.viewAftrLogin.hidden = true;
        self.homevc.lblWelcome.hidden = false;
        self.homevc.lblAnAccount.hidden = false;
        self.homevc.btnLogin.hidden = false;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"loginComplete" object:nil];

    }
    
    
}
- (void)openCloseMenu:(NSString*)isZero withView:(UIView*)customView{
   
    if([isZero isEqualToString:@"0"]){
        [UIView animateWithDuration:0.4 animations:^{
        CGRect frm = self.viewTab.frame;
        CGFloat fltWidth = ((self.view.frame.size.width * 80)/100);
        frm.origin.x = fltWidth;
        self.viewTab.frame = frm;
        self.viewTab.userInteractionEnabled = NO;
            if(customView==self.homevc.view){
                 self.homevc.transView.hidden = NO;
            }else if (customView==self.casinovc.view){
                 self.casinovc.viewTrans.hidden = NO;
            }else if (customView==self.diningvc.view){
                self.diningvc.viewTrans.hidden = NO;
            }else if (customView==self.eventvc.view){
                self.eventvc.viewTrans.hidden = NO;
            }else if (customView==self.hotelvc.view){
                self.hotelvc.viewTrans.hidden = NO;
            }else if (customView==self.nightvc.view){
                self.nightvc.viewTrans.hidden = NO;
            }else if (customView==self.offervc.view){
                self.offervc.viewTrans.hidden = NO;
            }else if (customView==self.aboutVc.view){
                self.aboutVc.viewTrans.hidden = NO;
            }else if (customView==self.settingsvc.view){
                self.settingsvc.viewTrans.hidden = NO;
            }else if (customView==self.socialVc.view){
                self.socialVc.viewTrans.hidden = NO;
            }
            
        CGRect frm2 = customView.frame;
        frm2.origin.x = fltWidth;
        customView.frame = frm2;
        
//        customView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.0];
//        customView.opaque = NO;
        
        CGRect frm3 = self.menuvc.view.frame;
        frm3.origin.x = 0;
        self.menuvc.view.frame = frm3;
    }];
    }else{
        
       
        [UIView animateWithDuration:0.4 animations:^{
            CGRect frm = self.viewTab.frame;
            CGFloat fltWidth = ((self.view.frame.size.width * 80)/100);
            frm.origin.x = 0;
            self.viewTab.frame = frm;
            self.viewTab.userInteractionEnabled = YES;
            if(customView==self.homevc.view){
                self.homevc.transView.hidden = YES;
            }else if (customView==self.casinovc.view){
                self.casinovc.viewTrans.hidden = YES;
            }else if (customView==self.diningvc.view){
                self.diningvc.viewTrans.hidden = YES;
            }else if (customView==self.eventvc.view){
                self.eventvc.viewTrans.hidden = YES;
            }else if (customView==self.hotelvc.view){
                self.hotelvc.viewTrans.hidden = YES;
            }else if (customView==self.nightvc.view){
                self.nightvc.viewTrans.hidden = YES;
            }else if (customView==self.offervc.view){
                self.offervc.viewTrans.hidden = YES;
            }else if (customView==self.aboutVc.view){
                self.aboutVc.viewTrans.hidden = YES;
            }else if (customView==self.settingsvc.view){
                self.settingsvc.viewTrans.hidden = YES;
            }else if (customView==self.socialVc.view){
                self.socialVc.viewTrans.hidden = YES;
            }
            
            CGRect frm2 = customView.frame;
            frm2.origin.x = 0;
            customView.frame = frm2;
            
            CGRect frm3 = self.menuvc.view.frame;
            frm3.origin.x = -fltWidth;
            self.menuvc.view.frame = frm3;
        }];
    }
}
- (void)removeAllView{
   [self.homevc.view removeFromSuperview];
   [self.casinovc.view removeFromSuperview];
   [self.diningvc.view removeFromSuperview];
   [self.hotelvc.view removeFromSuperview];
   [self.eventvc.view removeFromSuperview];
   [self.offervc.view removeFromSuperview];
   [self.aboutVc.view removeFromSuperview];
   [self.socialVc.view removeFromSuperview];
   [self.settingsvc.view removeFromSuperview];
   [self.nightvc.view removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)scanAction{
    [self.viewTab.layer removeAllAnimations];
    CGRect frm = self.scannerVC.view.frame;
    frm.origin.x = 500;
    self.scannerVC.view.frame=frm;
    self.scannerVC.view.alpha = 0.0;
    [UIView animateWithDuration:0.8 animations:^{
        CGRect frm1 = self.scannerVC.view.frame;
        frm1.origin.x = 0;
        self.scannerVC.view.frame=frm1;
        self.scannerVC.view.alpha = 1.0;
        [self.view addSubview:self.scannerVC.view];
    }];
}
- (void)homeAction{
    self.btnHome.selected = YES;
    
    self.btnCasino.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    
    [self removeAllView];

    [self.view addSubview:self.homevc.view];
    [self.homevc setEventOnCompletion:^(id object, NSUInteger EventTypeH) {
        NSString *str = object;
        if(EventTypeH ==kActionTapMenu){
            [self openCloseMenu:str withView:self.homevc.view];
        }else if (EventTypeH==kActionCasino){
            [self casinoAction];
        }else if (EventTypeH==kActionDining){
            [self diningAction];
        }else if (EventTypeH==kActionEvent){
            [self eventAction];
        }else if (EventTypeH==kActionHotel){
            [self hotelAction];
        }else if (EventTypeH==kActionNight){
            [self nightAction];
        }else if (EventTypeH==kActionSocial){
            [self socialAction];
            // [self performSegueWithIdentifier:@"segueSocial" sender:self];
        }else if (EventTypeH==kActionNotify){
            [self notifyAction];
        }else if (EventTypeH==kActionTap){
            [self openCloseMenu:str withView:self.homevc.view];
        }else if (EventTypeH==kActionAccount){
            [self loginAction];
        }else if (EventTypeH==kActionOffer){
            [self offerAction];
        }else if (EventTypeH==kScan){
            [self scanAction];
        }
    }];

}
- (void)casinoAction{
    self.btnCasino.selected = YES;
    
    self.btnHome.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    
     [self removeAllView];
     [self.view addSubview:self.casinovc.view];
  
    
    
    [self.casinovc setEventOnCompletion:^(id object, NSUInteger EventTypeC) {
        if(EventTypeC ==kCasTapMenu){
            NSString *str = object;
            [self openCloseMenu:str withView:self.casinovc.view];
        }else if (EventTypeC==kCasino){
            [self casinoAction];
        }else if (EventTypeC==kDining){
            [self diningAction];
        }else if (EventTypeC==kEvent){
            [self eventAction];
        }else if (EventTypeC==kHotel){
            [self hotelAction];
        }else if (EventTypeC==kNight){
            [self nightAction];
        }else if (EventTypeC==kDetails){
            delegate.strDetailsTitle = @"Casino";
          
            [self detailsAction];
        }else if (EventTypeC==kTap){
            NSString *str = object;
            [self openCloseMenu:str withView:self.casinovc.view];
        }else if (EventTypeC==kWinner){
            delegate.strDetailsTitle  = @"Recent Winners";
           
        }else if (EventTypeC==kNotifyC){
             [self notifyAction];
        }

    }];
}

- (void)detailsAction{
    self.detailsVc.imgView.image = [UIImage imageNamed:@"loading-big.png"];
    [self.viewTab.layer removeAllAnimations];
    CGRect frm = self.detailsVc.view.frame;
    frm.origin.x = 500;
    self.detailsVc.view.frame=frm;
    self.detailsVc.view.alpha = 0.0;
    [self.view addSubview:self.detailsVc.view];
    [UIView animateWithDuration:0.8 animations:^{
        
        [activity showView];
        UIFont *font = [UIFont fontWithName:@"Palatino-Roman" size:14.0];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                    forKey:NSFontAttributeName];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"strigil" attributes:attrsDictionary];
        
        self.detailsVc.lblNav.text = delegate.strDetailsTitle;
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:
                                               [delegate.page.strPageDesc dataUsingEncoding:NSUnicodeStringEncoding] options:
                                               @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                                           documentAttributes:nil error:nil];
        
        
        
        
        [attrStr enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrStr.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            
            UIFont* font = value;
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
            // font = [font fontWithSize:14.0];
            
            [attrStr removeAttribute:NSFontAttributeName range:range];
            [attrStr addAttribute:NSFontAttributeName value:font range:range];
        }];
        
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            NSString *strImage = [NSString stringWithFormat:@"https://landingpage.experiture.com/TrackingSystem/AG_44625/AD_44626/Images/%@",delegate.page.strPageMainImage];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:strImage]];
            
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activity hideView];
                    CGRect frm1 = self.detailsVc.view.frame;
                    frm1.origin.x = 0;
                    self.detailsVc.view.frame=frm1;
                    self.detailsVc.view.alpha = 1.0;
                    self.detailsVc.imgView.image = image;
                });
            }
        });
        
        
        self.detailsVc.lblTitle.text = delegate.page.strPageTitle;
        self.detailsVc.myText.attributedText = attrStr;
        [self.detailsVc.myText sizeToFit];
        
        
    }];
    
    
    
}

- (void)detailsAction1{
    self.detailsVc.imgView.image = [UIImage imageNamed:@"loading-big.png"];
    [self.viewTab.layer removeAllAnimations];
    CGRect frm = self.detailsVc.view.frame;
    frm.origin.x = 500;
    self.detailsVc.view.frame=frm;
    self.detailsVc.view.alpha = 0.0;
    [self.view addSubview:self.detailsVc.view];
    [UIView animateWithDuration:0.8 animations:^{
        
        
        
        
        [activity showView];
        UIFont *font = [UIFont fontWithName:@"Palatino-Roman" size:14.0];
        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:font
                                                                    forKey:NSFontAttributeName];
        NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"strigil" attributes:attrsDictionary];
        
        self.detailsVc.lblNav.text = delegate.strDetailsTitle;
        NSMutableAttributedString * attrStr = [[NSMutableAttributedString alloc] initWithData:
                                               [delegate.page.strPageDesc dataUsingEncoding:NSUnicodeStringEncoding] options:
                                               @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                                                                           documentAttributes:nil error:nil];
        
        
        
        
        [attrStr enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attrStr.length) options:0 usingBlock:^(id value, NSRange range, BOOL *stop) {
            
            UIFont* font = value;
            font = [UIFont fontWithName:@"HelveticaNeue-Light" size:15.0f];
            // font = [font fontWithSize:14.0];
            
            [attrStr removeAttribute:NSFontAttributeName range:range];
            [attrStr addAttribute:NSFontAttributeName value:font range:range];
        }];
        
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void) {
            
            NSString *strImage = [NSString stringWithFormat:@"https://landingpage.experiture.com/TrackingSystem/AG_44625/AD_44626/Images/%@",delegate.page.strPageMainImage];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:strImage]];
            
            UIImage* image = [[UIImage alloc] initWithData:imageData];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [activity hideView];
                    CGRect frm1 = self.detailsVc.view.frame;
                    frm1.origin.x = 0;
                    self.detailsVc.view.frame=frm1;
                    self.detailsVc.view.alpha = 1.0;
                    self.detailsVc.imgView.image = image;
                });
            }
        });
        
        
        self.detailsVc.lblTitle.text = delegate.page.strPageTitle;
        self.detailsVc.myText.attributedText = attrStr;
        [self.detailsVc.myText sizeToFit];

       
    }];

    
    
}
- (void)notifyAction{
    [self.viewTab.layer removeAllAnimations];
    CGRect frm = self.notifyVC.view.frame;
    frm.origin.x = 500;
    self.notifyVC.view.frame=frm;
    self.notifyVC.view.alpha = 0.0;
    [UIView animateWithDuration:0.8 animations:^{
        CGRect frm1 = self.notifyVC.view.frame;
        frm1.origin.x = 0;
        self.notifyVC.view.frame=frm1;
        self.notifyVC.view.alpha = 1.0;
        [self.view addSubview:self.notifyVC.view];
    }];

}

- (void)diningAction{
    self.btnDining.selected = YES;
    
    self.btnHome.selected = NO;
    self.btnCasino.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    
    [self removeAllView];
    [self.view addSubview:self.diningvc.view];
    
    [self.diningvc setEventOnCompletion:^(id object, NSUInteger EventTypeD) {
        if(EventTypeD ==kDinTapMenu){
            NSString *str = object;
            [self openCloseMenu:str withView:self.diningvc.view];
        }else if (EventTypeD==kDinCasino){
            [self casinoAction];
        }else if (EventTypeD==kDinDining){
            [self diningAction];
        }else if (EventTypeD==kDinEvent){
            [self eventAction];
        }else if (EventTypeD==kDinHotel){
            [self hotelAction];
        }else if (EventTypeD==kDinNight){
            [self nightAction];
        }else if (EventTypeD==kDinDetails){
            delegate.strDetailsTitle = @"Dining";
            delegate.page = object;
            [self detailsAction];
        }else if (EventTypeD==kDinTap){
            NSString *str = object;
            [self openCloseMenu:str withView:self.diningvc.view];
        }else if (EventTypeD==kNotifyD){
            [self notifyAction];
        }

    }];
    
}

- (void)eventAction{
    self.btnEvent.selected = YES;
    
    self.btnHome.selected = NO;
    self.btnCasino.selected = NO;
    self.btnDining.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    
    [self removeAllView];
    [self.view addSubview:self.eventvc.view];
    
    [self.eventvc setEventOnCompletion:^(id object, NSUInteger EventTypeE) {
        
        if(EventTypeE ==kEventTapMenu){
            NSString *str = object;
            [self openCloseMenu:str withView:self.eventvc.view];
        }else if (EventTypeE==kEventCasino){
            [self casinoAction];
        }else if (EventTypeE==kEventDining){
            [self diningAction];
        }else if (EventTypeE==kEventEvent){
            [self eventAction];
        }else if (EventTypeE==kEventHotel){
            [self hotelAction];
        }else if (EventTypeE==kEventNight){
            [self nightAction];
        }else if (EventTypeE==kEventDetails){
            delegate.strDetailsTitle = @"Events";
            delegate.page = object;
             [self detailsAction];
        }else if (EventTypeE==kEventTap){
            NSString *str = object;
            [self openCloseMenu:str withView:self.eventvc.view];
        }else if (EventTypeE==kEventNotify){
            [self notifyAction];
        }

    }];

}

- (void)hotelAction{
    self.btnHotel.selected = YES;
    
    self.btnHome.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnCasino.selected = NO;
    self.btnOffer.selected = NO;
    
    
    [self removeAllView];
    [self.view addSubview:self.hotelvc.view];
    [self.hotelvc setEventOnCompletion:^(id object, NSUInteger EventTypeHot) {
       
        if(EventTypeHot ==kHotelTapMenu){
             NSString *str = object;
            [self openCloseMenu:str withView:self.hotelvc.view];
        }else if (EventTypeHot==kHotelCasino){
            [self casinoAction];
        }else if (EventTypeHot==kHotelDining){
            [self diningAction];
        }else if (EventTypeHot==kHotelEvent){
            [self eventAction];
        }else if (EventTypeHot==kHotelHotel){
            [self hotelAction];
        }else if (EventTypeHot==kHotelNight){
            [self nightAction];
        }else if (EventTypeHot==kHotelDetails){
            delegate.strDetailsTitle  = @"Hotel";
            delegate.page = object;
            [self detailsAction];
           
        }else if (EventTypeHot==kReserve){
            delegate.strDetailsTitle  = @"Reservations";
            [self reserveAction];
           
        }
        else if (EventTypeHot==kHotelTap){
             NSString *str = object;
            [self openCloseMenu:str withView:self.hotelvc.view];
        }else if (EventTypeHot==kHotelNotify){
            [self notifyAction];
        }

    }];
}
- (void)offerAction{
    self.btnOffer.selected = YES;
    
    self.btnHome.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnCasino.selected = NO;
    
    
    [self removeAllView];
    [self.view addSubview:self.offervc.view];
    [self.offervc setEventOnCompletion:^(id object, NSUInteger EventTypeO) {
        NSString *str = object;
        if(EventTypeO ==kOfferTapMenu){
            [self openCloseMenu:str withView:self.offervc.view];
        }else if (EventTypeO==kOfferCasino){
            [self casinoAction];
        }else if (EventTypeO==kOfferDining){
            [self diningAction];
        }else if (EventTypeO==kOfferEvent){
            [self eventAction];
        }else if (EventTypeO==kOfferHotel){
            [self hotelAction];
        }else if (EventTypeO==kOfferNight){
            [self nightAction];
        }else if (EventTypeO==kOfferDetails){
            delegate.strDetailsTitle  = @"Offer";
             [self detailsAction];
        }else if (EventTypeO==kOfferTap){
            [self openCloseMenu:str withView:self.offervc.view];
        }else if (EventTypeO==kOfferScan){
            [self scanAction];
        }else if (EventTypeO==kOfferNotify){
            [self notifyAction];
        }

    }];

}
- (void)nightAction{
    self.btnHome.selected = YES;
    
    self.btnCasino.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    
    [self removeAllView];
    [self.view addSubview:self.nightvc.view];
    [self.nightvc setEventOnCompletion:^(id object, NSUInteger EventTypeN) {
        
        if(EventTypeN ==kNightTapMenu){
            NSString *str = object;
            [self openCloseMenu:str withView:self.nightvc.view];
        }else if (EventTypeN==kNightCasino){
            [self casinoAction];
        }else if (EventTypeN==kNightDining){
            [self diningAction];
        }else if (EventTypeN==kNightEvent){
            [self eventAction];
        }else if (EventTypeN==kNightHotel){
            [self hotelAction];
        }else if (EventTypeN==kNightNight){
            [self nightAction];
        }else if (EventTypeN==kNightDetails){
            delegate.strDetailsTitle  = @"Nightlife";
            delegate.page = object;
             [self detailsAction];
        }else if (EventTypeN==kNightTap){
            NSString *str = object;
            [self openCloseMenu:str withView:self.nightvc.view];
        }else if (EventTypeN==kNightNotify){
            [self notifyAction];
        }

    }];
}
-(void)aboutAction{
    self.btnHome.selected = YES;
    
    self.btnCasino.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    [self removeAllView];
    [self.view addSubview:self.aboutVc.view];
    [self.aboutVc setEventOnCompletionS:^(id object, NSUInteger EventTypeA) {
        NSString *str = object;
        if(EventTypeA==kAboutMenu){
             [self openCloseMenu:str withView:self.aboutVc.view];
        }else if (EventTypeA==kAboutTap){
            [self openCloseMenu:str withView:self.aboutVc.view];
        }else if (EventTypeA==kAboutNotify){
            [self notifyAction];
        }
    }];

}
-(void)settingsAction{
    self.btnHome.selected = YES;
    
    self.btnCasino.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    [self removeAllView];
    [self.view addSubview:self.settingsvc.view];
    [self.settingsvc setEventOnCompletionSt:^(id object, NSUInteger EventTypeS) {
        NSString *str = object;
        if(EventTypeS==kMenuSt){
            [self openCloseMenu:str withView:self.settingsvc.view];
        }else if (EventTypeS==kTapSt){
            [self openCloseMenu:str withView:self.settingsvc.view];
        }else if (EventTypeS==kStNotify){
            [self notifyAction];
        }
    }];
}
- (void)socialAction{
    self.btnHome.selected = YES;
    
    self.btnCasino.selected = NO;
    self.btnDining.selected = NO;
    self.btnEvent.selected = NO;
    self.btnHotel.selected = NO;
    self.btnOffer.selected = NO;
    [self removeAllView];
    [self.view addSubview:self.socialVc.view];
    [self.socialVc setEventOnCompletionS:^(id object, NSUInteger EventTypeS) {
        NSString *str = object;
        if(EventTypeS == kSocialMenu){
             [self openCloseMenu:str withView:self.socialVc.view];
        }else if (EventTypeS==kSocialNotify){
            [self notifyAction];
        }
        else{
             [self openCloseMenu:str withView:self.socialVc.view];
        }
    }];
}

- (IBAction)selectedTab:(id)sender {
    UIButton *tabBtn = (UIButton *)sender;
    if(tabBtn.tag==0){
        [self homeAction];
    }else if (tabBtn.tag==1){
        [self casinoAction];
    }else if (tabBtn.tag==2){
        [self diningAction];
    }else if (tabBtn.tag==3){
        [self eventAction];
    }else if (tabBtn.tag==4){
        [self hotelAction];
    }else if (tabBtn.tag==5){
        
        //[self offerAction];
     //Login or logout
     NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
     NSString *strUname = [prefs stringForKey:@"username"];
     if(strUname==nil){
         [self loginAction];
     }else{
        [self offerAction];
     }
    }
    
    //[self setSelectedIndex:tabBtn.tag];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
