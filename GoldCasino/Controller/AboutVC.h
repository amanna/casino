//
//  AboutVC.h
//  GoldCasino
//
//  Created by Amrita on 23/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeA {
    kAboutMenu = 0,
    kAboutTap = 1,
    kAboutNotify=2
};
typedef void (^EventCompletionHandlerA)(id object, NSUInteger EventTypeA);
@interface AboutVC : UIViewController{
    EventCompletionHandlerA eventCompletionHandlerA;
}
- (void)setEventOnCompletionS:(EventCompletionHandlerA)handler ;
@property (weak, nonatomic) IBOutlet UIView *viewTrans;

@end
