//
//  UtiltiyManager.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Susim Samanta. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtiltiyManager : NSObject
+ (void)showAlertWithMessage:(NSString *)message;
+(NSData *)getImageDataWithFileName:(NSString *)fileName;
+(void)storeImageData:(NSData *)imageData withName:(NSString *)fileName;
+ (BOOL)isReachableToInternet;
+(NSString *)decodeHTMLCharacterEntities:(NSString*)str;

+(BOOL)validateEmail: (NSString *) candidate;
+(void)saveWatchData:(NSDictionary *)dict;
+(NSDictionary *)getWatchData;
+(NSString *)appendHTTPOnString:(NSString *)urlString;
@end
