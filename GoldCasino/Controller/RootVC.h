//
//  RootVC.h
//  GoldCasino
//
//  Created by Amrita on 25/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
enum EventTypeR {
    kMenuAction = 0,
    kNotifyAction = 1
};
typedef void (^EventCompletionHandlerR)(id object, NSUInteger EventTypeR);
@interface RootVC : UIViewController{
    EventCompletionHandlerR eventCompletionHandlerR;
}
@property (weak, nonatomic) IBOutlet UIView *viewTab;
@property (weak, nonatomic) IBOutlet UIButton *btnCasino;
@property (weak, nonatomic) IBOutlet UIButton *btnHome;
@property (weak, nonatomic) IBOutlet UIButton *btnDining;
@property (weak, nonatomic) IBOutlet UIButton *btnEvent;
@property (weak, nonatomic) IBOutlet UIButton *btnHotel;
@property (weak, nonatomic) IBOutlet UIButton *btnOffer;
- (void)setEventOnCompletionR:(EventCompletionHandlerR)handler ;
-(void)addHome;
@end
