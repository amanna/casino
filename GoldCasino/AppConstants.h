#define kBaseURL @"http://api.experiture.com"

#define kApiKey  @"27CCC3A558E542DD827649EED27F64C3"

#define kLogin @"/rest/v1/json/authenticateUserRest"

#define kAllOfferList @"/rest/v1/json/getOfferDetailListRest"
#define kMyOfferList @"/rest/v1/json/getMyOfferListRest"
#define kSaveOffer @"/rest/v1/json/saveMyOfferRest"
#define kScanOffer @"/rest/v1/json/ScanOfferQRCodeRest"

#define kSaveDeviceToken @"/rest/v1/json/saveDeviceToken"
#define kLocDeviceToken @"/rest/v1/json/getGeoLocationDataRest"

#define kSectionList @"/rest/v1/json/GetSectionList"
#define kSubSectionList @"/rest/v1/json/GetSubsectionDetails"
#define kPageList @"/rest/v1/json/GetPageDetails"
#define kPageContentDetails @"/rest/v1/json/GetPageContentDetails"


#define kGetUserSettings @"/rest/v1/json/GetUserSettings"
#define kUpdateUserSettings @"/rest/v1/json/UpdateSettings"


#define kSectionPageList @"/rest/v1/json/GetSubSectionWithPage"

#define kPostURL @"get_recent_posts"
#define kPostDetails @"get_post/?post_id="
#define kPostDetailsType @"get_post/?"
#define kSearch @"get_search_results/?search="
#define kMenuDetails @"get_category_posts/?category_id="
#define kComment @"respond/submit_comment/?"
#define kPostURLPage @"get_recent_posts/?page="
#define kTagDetails @"get_tag_posts/?tag_id="
#define kNotificationURL @"/push_notifications/get_push_categories/"

#define iPhone UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPhone
#define iPad UI_USER_INTERFACE_IDIOM()== UIUserInterfaceIdiomPad

#define kOptions @"options/get_options/?options=wovax_app_home_page_id,wovax_app_home_screen,wovax_app_home_webview_url,wovax_phone_portrait_banner,wovax_link_banner,wovax_ad_option,wovax_html_custom_ad,wovax_js_custom_ad,wovax_google_analytics_id,wovax_excluded_categories,wovax_ios_nav_bar_color,wovax_ios_nav_buttons_color,wovax_ios_nav_title_color,wovax_ios_status_bar_color,wovax_app_name,wovax_ios_webview_tab_bar_buttons_color" // 16 parameters, maximum is 20

#define kOptions2 @"options/get_options/?options=wovax_ios_menu_home_button,wovax_ios_back_button,wovax_ios_save_button,wovax_ios_comment_button,wovax_ios_post_comment_placeholder,wovax_ios_settings_title,wovax_ios_menu_highlight_background_color,wovax_ios_menu_background_color,wovax_ios_menu_divide_color,wovax_ios_menu_highlight_text_color,wovax_ios_menu_text_color,wovax_ios_search_bar_color,wovax_ios_nav_settings_icon,wovax_ios_nav_menu_icon,wovax_ios_ipad_segment_posts,wovax_ios_ipad_segment_menu" // 15 parameters, maximum is 20

#define kOptions3 @"options/get_options/?options=" // 0 parameters, maximum is 20


#define kIDXOptions @"options/get_options/?options=wovax_idx_type_filter,wovax_idx_state_city_filter,wovax_idx_zipcode_filter,wovax_idx_minimum_price_filter,wovax_idx_minimum_price_filter_options,wovax_idx_square_footage_filter,wovax_idx_square_footage_filter_options,wovax_idx_acreage_filter,wovax_idx_acreage_filter_options,wovax_idx_bedrooms_filter,wovax_idx_bedrooms_filter_options,wovax_idx_keywords_filter" // 12 parameters, maximum is 20

#define kLegalIDXOption @"/options/get_options/?options=wovax_idx_legal_page_id"
#define kRegisterAPNS @"/push_notifications/register_device/?platform=apns_development&deviceId="
#define kUnsubscribeAPNS @"/push_notifications/blacklist_categories/?platform=apns&deviceId="
#define kClearBadgeToken @"/?wovaxmenu=ios_clear_badge&deviceId="
#define kMap @"/get_posts/?meta_key=geo_enabled&meta_value=1&count=150"

#define kNoInternet @"No Internet! Please connect to a wifi network or activate cellular data internet."

#define kPropertyFilterURL  @"/private_key/idx/idx_filter/"