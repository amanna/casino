//
//  Pages.h
//  GoldCasino
//
//  Created by MAPPS MAC on 08/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pages : NSObject
@property(nonatomic)NSString *strPageId;
@property(nonatomic)NSString *strPageDesc;
@property(nonatomic)NSString *strPageMainImage;
@property(nonatomic)NSString *strPageThumbImg;
@property(nonatomic)NSString *strPageTitle;
@property(nonatomic)NSString *strSubTitle;
@property(nonatomic)NSString *strPageType;
- (id)initWithDictionary:(NSDictionary *)dict;
@end
