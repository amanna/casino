//
//  Offer.m
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Events.h"

@implementation Events

- (id)initWithDictionary:(NSDictionary *)dict{
    if (self = [super init]) {
               
    }
    return self;
}
- (id)constructEvent:(NSInteger)index{
    switch (index) {
        case 0:{
            self.strTitle = @"Halloween";
            self.strSubTitle = @"Sat Oct 31 2015 09:00 AM ";
            self.strDesc = @"<html><body>This Halloween get ready to get spooked with special Halloween themed décor and specials throughout the property. Stop by for a free beer when you show up to Sky Bar in costume, and enter the XGold casino-wide costume contest! We will also be featuring Halloween specials at several restaurants on the property and keep an eye out for roaming goblins, witches, and zombies!</body></html>";
            self.strImage = @"halloween.png";
            self.strLargeImage = @"halloween-big.png";
        }
            break;
        case 1:{
            self.strTitle = @"The Summit 2015";
            self.strSubTitle = @"Mon Nov 08 2015 10:00 AM";
            self.strDesc = @"<html><body>The Summit 2015 is a chance for all of our associates, stakeholders, partners, and friends to learn about what new and exciting things are coming up for XGOLD Casino and to review the past year in depth. We are excited to present president Seth Freeman, who will present the keynote at 2pm, concluding the conference. Request an invite by emailing summit@xgoldcasino.com.</body></html>";
            self.strImage = @"TheSummit.png";
            self.strLargeImage = @"TheSummit-big.png";
        }
            break;
        case 2:{
            self.strTitle = @"Nobel Aircare";
            self.strSubTitle = @"Sat Nov 21 2015 09:00 AM";
            self.strDesc = @"<html><body>Our annual national conference is the place to be to meet leaders of volunteer pilot organizations nationwide, share ideas, and learn the latest news about Public Benefit Flying. As part of our Air Care conferences we have wonderful social events in the venue and nearby areas. You will be happy to hear that Air Care 2015, your premier event for leaders of all Public Benefit Flying Organizations, will be held at XGold Casino on November 21st, 2015.</body></html>";
            self.strImage = @"NobelAirCare.png";
            self.strLargeImage = @"NobelAirCare-big.png";
        }
            break;
            
        default:
            break;
    }
    return self;

    
}
@end
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}

