//
//  Hotel.h
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 29/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hotel : NSObject
@property(nonatomic)NSString *strTitle;
@property(nonatomic)NSString *strSubTitle;
@property(nonatomic)NSString *strDesc;
@property(nonatomic)NSString *strImage;
@property(nonatomic)NSString *strLargeImage;

- (id)initWithDictionary:(NSDictionary *)dict;
- (id)constructHotel:(NSInteger)index;

@end
