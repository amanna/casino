//
//  Gallery.m
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 29/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Gallery.h"

@implementation Gallery
- (id)initWithDictionary:(NSDictionary *)dict{
    
    return self;
}
- (id)constructGallery:(NSInteger)index{
    
    switch (index) {
        case 0:{
            self.strTitle = @"Table Games";
            self.strSubTitle = @"Blackjack, Baccarat, Craps...";
            self.strDesc = @"<html><body>Test your luck at one of over 200 table games on our property. We have Blackjack, Spanish 21, Caribbean Stud Poker, Baccarat, Craps, and Roulette in addition to our exciting 42-table Poker Room. Grab a seat to get in on the action! </body></html>";
            self.strImage = @"casino-tablegame.png";
            self.strLargeImage = @"casino-tablegame-big.png";
        }
        break;
        case 1:{
            self.strTitle = @"Roulette";
            self.strSubTitle = @"High Tech Roulette games...";
            self.strDesc = @"<html><body>Roulette is an exciting, fast paced game with a variety of bets and payoffs. Be sure to check out our 14 live Roulette tables in the Sky Casino, or try your hand at virtual roulette at our cutting edge e-tables in the West wing. </body></html>";
            self.strImage = @"casino-roulette.png";
             self.strLargeImage = @"casino-roulette-big.png";
        }
            break;
        case 2:{
            self.strTitle = @"Progressive Jackpots";
            self.strSubTitle = @"Current progressive jackpots...";
            self.strDesc = @"<html><body>Let the bells and whistles of our 2,500 slot machines excite your senses with fun games and chances to win big. With our cutting edge technology, we have interactive video games and mechanical slots to entertain every player, whether penny slots or $100 games are your pleasure.</body></html>";
            self.strImage = @"casino-progressive.png";
             self.strLargeImage = @"casino-progressive-big.png";
        }
            break;
        case 3:{
            self.strTitle = @"Recent Winners";
            self.strSubTitle = @"XGold Casino Celebrates...";
            self.strDesc = @"<html><body>Amanda Simmons from Fort Lauderdale, Florida won big on Wednesday when she hit a jackpot for over 1.8 million on a Wide-Area Progressive penny slot. This makes Amanda one of our highest winners to date at this casino location. She is elated with her big win and excellent luck at XGold Casino. </body></html>";
            self.strImage = @"casino-recent-winner.png";
            self.strLargeImage = @"casino-recent-winner-big.png";
        }
            break;
        case 4:{
            self.strTitle = @"Featured Players";
            self.strSubTitle = @"Top Earning Players at XGold...";
            self.strDesc = @"<html><body>Amanda Simmons from Fort Lauderdale, Florida won big on Wednesday when she hit a jackpot for over 1.8 million on a Wide-Area Progressive penny slot. This makes Amanda one of our highest winners to date at this casino location. She is elated with her big win and excellent luck at XGold Casino. </body></html>";
            self.strImage = @"casino-feature-player.png";
            self.strLargeImage = @"casino-feature-player-big.png";
        }
            break;


        default:
            break;
    }
    return self;
}
@end
