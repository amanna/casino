//
//  Offer.m
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Offer.h"

@implementation Offer

- (id)initWithDictionary:(NSDictionary *)dict{
    if (self = [super init]) {
        self.strOfferId = [dict valueForKey:@"OfferId"];
        self.strOfferName = [dict valueForKey:@"OfferName"];
        self.strOfferDesc = [dict valueForKey:@"OfferDescription"];
        self.strOffervalidityperiod = [dict valueForKey:@"OfferValidityPeriod"];
        self.strOfferExp = [dict valueForKey:@"OfferExpDate"];
        self.strOfferImage = [dict valueForKey:@"OfferImage"];
        
    }
    return self;
}
@end
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}

