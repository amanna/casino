//
//  NightLife.m
//  GoldCasino
//
//  Created by MAPPS MAC on 29/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Night.h"

@implementation Night
- (id)constructDinner:(NSInteger)index{
    switch (index) {
        case 0:{
            self.strTitle = @"SkyBar";
            self.strSubTitle = @"Enjoy rooftop cocktails with a view.... ";
            self.strDesc = @"<html><body><p><strong>Hours of Operation</strong></p><p>5pm - 1 am (Monday - Thursday)</p><p>3pm &ndash; 2am (Friday &amp; Saturday)</p><p>Closed (Sunday)</p><strong>About us</strong><p>Enjoy rooftop cocktails with a view at XGold Casino’s newest bar.  SkyBar features a glitzy ambiance with premium cocktails to get the night going.  Stop by for drink specials before 7pm every night, or come late to catch a live DJ set.</p></body></html>";
            self.strImage = @"skybar.png";
            self.strLargeImage = @"skybar-big.png";
        }
            break;
        case 1:{
            self.strTitle = @"Bar Note";
            self.strSubTitle = @"Live music every night…";
            self.strDesc = @"<html><body><p><strong>Hours of Operation</strong></p><p>6pm - 1 am (Sunday to Thursday)</p><p>5pm &ndash; 2am (Friday &amp; Saturday)</p><strong>About us</strong><p>Catch an incredible live show at Bar Note any day of the week.  We feature live Jazz on Sundays and Thursdays, Classic Rock on Mondays, Wednesdays, and Saturdays, and special guests on Tuesdays and Fridays.  Check our twitter feed @BarNote for our up-to-date schedule of live bands and featured performers.</p></body></html>";
            self.strImage = @"barnote.png";
            self.strLargeImage = @"barnote-big.png";
        }
            break;
        case 2:{
            self.strTitle = @"Luxe Nightclub";
            self.strSubTitle = @"Dance the night away…";
            self.strDesc = @"<html><body><p><strong>Hours of Operation</strong></p><p>10pm - 2 am (Thursday - Saturday)</p><strong>About us</strong><p>Dance the night away with our resident DJ GoldRX spinning top 40 each Thursday, Friday, and Saturday night.  Be sure to come by for Throwback Thursday each week to dance to hits from the 80’s, 90’s, and 00’s all night long.</p></body></html>";
            self.strImage = @"luxe.png";
            self.strLargeImage = @"luxe-big.png";
        }
            break;
            
        default:
            break;
    }
    return self;
    
    
}

@end
