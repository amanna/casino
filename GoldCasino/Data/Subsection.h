//
//  Subsection.h
//  GoldCasino
//
//  Created by MAPPS MAC on 08/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Subsection : NSObject
@property(nonatomic)NSString *strSubSectionId;
@property(nonatomic)NSString *strSubSectionTitle;
- (id)initWithDictionary:(NSDictionary *)dict;

@end
