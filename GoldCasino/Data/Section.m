//
//  Section.m
//  GoldCasino
//
//  Created by MAPPS MAC on 07/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Section.h"

@implementation Section
- (id)initWithDictionary:(NSDictionary *)dict{
    self.strSectionId= [dict valueForKey:@"SectionId"];
    self.strSectionTitle= [dict valueForKey:@"SectionName"];
    self.strSectionThumb= [dict valueForKey:@"SectionThumbnailUrl"];
    self.strSectionMain= [dict valueForKey:@"SectionImageUrl"];
    return self;
}
@end
