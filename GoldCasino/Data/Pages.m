//
//  Pages.m
//  GoldCasino
//
//  Created by MAPPS MAC on 08/03/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "Pages.h"

@implementation Pages
- (id)initWithDictionary:(NSDictionary *)dict{
    self.strPageId= [dict valueForKey:@"PageId"];
    self.strPageDesc= [dict valueForKey:@"PageDesc"];
    self.strPageTitle= [dict valueForKey:@"PageTitle"];
    self.strPageThumbImg= [dict valueForKey:@"PageThumbImg"];
    return self;
}
@end
