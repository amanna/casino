//
//  AppDelegate.m
//  GoldCasino
//
//  Created by MAPPS MAC on 11/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import "AppDelegate.h"
#import "WebServiceManager.h"
#import "MenuVC.h"
#import "RootVC.h"
#import "POAcvityView.h"
@interface AppDelegate (){
    POAcvityView *activity;
}
@property(nonatomic)MenuVC *menuvc;
@property(nonatomic)RootVC *rootvc;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //[self createTab];
    // Let the device know we want to receive push notifications
    self.arrSection = [[NSMutableArray alloc]init];
    self.page = [[Pages alloc]init];
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert)];
    }

   [self createMenu];
    
      
    return YES;
}
- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content---%@", token);
    NSString* Identifier = [[[UIDevice currentDevice] identifierForVendor] UUIDString]; // IOS 6+
    NSLog(@"output is : %@", Identifier);
    [[NSUserDefaults standardUserDefaults] setObject:token forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] setObject:Identifier forKey:@"udid"];
  
   [WebServiceManager saveMyDeviceToken:@"hh" onCompletion:^(id object, NSError *error) {
       if(object){
           
       }else{
           
       }
   }];
   // NSLog(@"My token is: %@", deviceToken);
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo{
    NSLog(@"Received notification: %@", userInfo);
    //Get the documents directory path
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:@"offer.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager fileExistsAtPath: path]) {
        
        path = [documentsDirectory stringByAppendingPathComponent: [NSString stringWithFormat:@"offer.plist"] ];
    }
}

- (void)createMenu{
    UIStoryboard *storyboard =
    [UIStoryboard storyboardWithName:@"Main"
                              bundle:nil];

    self.menuvc =
    [storyboard instantiateViewControllerWithIdentifier:@"MenuVC"];
    
    self.rootvc =
    [storyboard instantiateViewControllerWithIdentifier:@"RootVC"];
    
//   UINavigationController *navRoot = [[UINavigationController alloc]initWithRootViewController:self.rootvc];
//   navRoot.navigationBar.hidden = true;
    self.window.rootViewController = self.rootvc;
    [self.window makeKeyAndVisible];
    
    
}
- (void)disAppearMenu:(NSInteger)indexMenu withVc:(UIViewController *)vc{
    UIView *view;

    [UIView animateWithDuration:0.4 animations:^{
        CGRect frm = view.frame;
        frm.origin.x = 0;
        view.frame = frm;
    } completion:^(BOOL finished) {
        self.window.rootViewController = _customTabBarVC;
//        
//        0- Home
//        1- Casino
//        2- Dining
//        3- Event
//        4- Hotel
//        5- Offer
        if(indexMenu==0){
            [_customTabBarVC processCompleted:0];
        }else if (indexMenu==1){
            [_customTabBarVC processCompleted:5];
        }else if (indexMenu==2){
            [_customTabBarVC processCompleted:1];
        }else if (indexMenu==3){
             [_customTabBarVC processCompleted:2];
        }else if (indexMenu==4){
             [_customTabBarVC processCompleted:3];
        }else if (indexMenu==5){
             [_customTabBarVC processCompleted:0];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"NightNotification"
             object:self];
        }else if (indexMenu==6){
            [_customTabBarVC processCompleted:4];
        }else if (indexMenu==7){
            [_customTabBarVC processCompleted:0];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"AboutNotification"
             object:self];
        }else if (indexMenu==8){
            [_customTabBarVC processCompleted:0];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"SocialNotification"
             object:self];
        }else if (indexMenu==9){
            [_customTabBarVC processCompleted:0];
            [[NSNotificationCenter defaultCenter]
             postNotificationName:@"SettingsNotification"
             object:self];
        }
        
    }];
    
}
- (void)slideMenu:(NSInteger)isSlide withVc:(UIViewController *)vc{
    UIView *view = vc.view;
//    if([vc isKindOfClass:[HomeVC class]]){
//        view = self.homevc.view;
//    }else if([vc isKindOfClass:[CasinoVC class]]){
//        view = self.casinovc.view;
//    }else if([vc isKindOfClass:[DiningVC class]]){
//        view = self.diningvc.view;
//    }else if([vc isKindOfClass:[HotelVC class]]){
//        view = self.hotelvc.view;
//    }else if([vc isKindOfClass:[EventVC class]]){
//        view = self.eventvc.view;
//    }else if([vc isKindOfClass:[OfferVC class]]){
//        view = self.offervc.view;
//    }
    if(isSlide==0){
            [self.rootvc.view addSubview:self.menuvc.view];
            [UIView animateWithDuration:0.4 animations:^{
            CGRect frm = view.frame;
            CGFloat fltWidth = ((view.frame.size.width * 80)/100);
            frm.origin.x = fltWidth;
            view.frame = frm;
                
            CGRect frm2 = self.rootvc.viewTab.frame;
                frm2.origin.x = fltWidth;
                self.rootvc.viewTab.frame = frm2;

                 CGRect frm1 = self.menuvc.view.frame;
                frm1.origin.x = 0;
                self.menuvc.view.frame = frm1;
            
        }];

    }else{
        [UIView animateWithDuration:0.4 animations:^{
            CGRect frm = view.frame;
            frm.origin.x = 0;
            view.frame = frm;
        } completion:^(BOOL finished) {
            
        }];
        
    }
}



- (void)createTab{
//    _customTabBarVC = [[SSTabBarController alloc] initWithNibName:@"SSTabBarController" bundle:nil];
//    UIStoryboard *storyboard =
//    [UIStoryboard storyboardWithName:@"Main"
//                              bundle:nil];
//   HomeVC *homevc =
//    [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
//    UINavigationController *navHome = [[UINavigationController alloc]initWithRootViewController:homevc];
//    navHome.navigationBarHidden = YES;
//    
//   CasinoVC *casinoVC =
//   [storyboard instantiateViewControllerWithIdentifier:@"CasinoVC"];
//   UINavigationController *navCasino = [[UINavigationController alloc]initWithRootViewController:casinoVC];
//   navCasino.navigationBarHidden = YES;
//    
//    DiningVC *diningVc =
//    [storyboard instantiateViewControllerWithIdentifier:@"DiningVC"];
//    UINavigationController *navDining = [[UINavigationController alloc]initWithRootViewController:diningVc];
//    navDining.navigationBarHidden = YES;
//    
//    EventVC *eventVc =
//    [storyboard instantiateViewControllerWithIdentifier:@"EventVC"];
//    UINavigationController *navEvent = [[UINavigationController alloc]initWithRootViewController:eventVc];
//    navEvent.navigationBarHidden = YES;
//    
//    
//    HotelVC *hotelVc =
//    [storyboard instantiateViewControllerWithIdentifier:@"HotelVC"];
//    UINavigationController *navHotel = [[UINavigationController alloc]initWithRootViewController:hotelVc];
//    navHotel.navigationBarHidden = YES;
//
//    
//    OfferVC *offerVc =
//    [storyboard instantiateViewControllerWithIdentifier:@"OfferVC"];
//    UINavigationController *navOffer = [[UINavigationController alloc]initWithRootViewController:offerVc];
//    navOffer.navigationBarHidden = YES;
//    
//    _customTabBarVC.viewControllers = [NSArray arrayWithObjects:navHome,navCasino,navDining,navEvent,navHotel ,navOffer,nil];
//    self.window.rootViewController = _customTabBarVC;
//    _customTabBarVC.selectedViewController = navHome;
//    [self.window makeKeyAndVisible];
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
