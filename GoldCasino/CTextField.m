//
//  CTextField.m
//  BabySitterHawaii
//
//  Created by debashisandria on 20/02/15.
//  Copyright (c) 2015 Digicrazers. All rights reserved.
//

#import "CTextField.h"

@implementation CTextField
@synthesize leftPadding;

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, leftPadding, self.frame.size.height)];
    self.leftView = paddingView;
    self.leftViewMode = UITextFieldViewModeAlways;
}





@end
