//
//  AppDelegate.h
//  GoldCasino
//
//  Created by MAPPS MAC on 11/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SSTabBarController.h"
#import "Pages.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic) SSTabBarController *customTabBarVC;
@property(nonatomic)NSString *strDetailsTitle;
@property(nonatomic)Pages *page;
@property(nonatomic)NSMutableArray *arrSection;
@end

