//
//  OfferCell.h
//  GoldCasino
//
//  Created by SKETCH_IOS_01 on 27/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OfferCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgOffer;
@property (weak, nonatomic) IBOutlet UILabel *TitleOffer;
@property (weak, nonatomic) IBOutlet UILabel *subTitleOffer;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

@end
