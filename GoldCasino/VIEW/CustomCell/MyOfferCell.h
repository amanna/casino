//
//  MyOfferCell.h
//  GoldCasino
//
//  Created by Amrita on 28/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOfferCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgOffer;
@property (weak, nonatomic) IBOutlet UILabel *TitleOffer;
@property (weak, nonatomic) IBOutlet UILabel *subTitleOffer;
@end
