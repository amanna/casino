//
//  GoldCell.h
//  GoldCasino
//
//  Created by MAPPS MAC on 19/01/16.
//  Copyright © 2016 ___ASDASDAFULLUSERNAME__SAD_. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoldCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgGallery;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@end
